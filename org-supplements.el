;;; org-supplements.el --- Supplements for Org mode  -*- lexical-binding: t -*-

;; #+STARTUP: oddeven
;; #+options: toc:2
;; 
;;; Header:                                                           :noexport:

;; Copyright 2014-2016 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl <marcowahlsoft@gmail.com>
;; Created: 2014
;; Version: see VCS
;; Keywords: convenience, helpers
;; URL: https://marcowahl.github.io/ https://github.com/marcowahl/little-helpers

;; This file is not part of Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; 
;;; Commentary:

;; This file contains a bunch of supplements for Org mode.

;;  - Expect changes any time!

;;  - This file contains sections for certain functionality.

;; This emacs-lisp file started in lentic style.  See
;; https://github.com/phillord/lentic.

;; Wish: get back to lentic.

;; ** Install

;; #+BEGIN_SRC text
;; (push "...path/to/this/file" load-path)
;; (require 'org-supplements.org)
;; #+END_SRC

;; ** Dependencies

;; The Org package needs to be in the right place.
;; 
;;; Features:
;; :PROPERTIES:
;; :ID:       c2d9fb34-7490-427a-93b8-662e0b768166
;; :END:

;; 
;; ** Save textproperty-images to hashed

;; #+BEGIN_SRC emacs-lisp

(require 'org)
(require 'org-attach)

(defun xxx-mw-next-property-display-data (start limit)
  "Move point to the next property-display location."
  (let ((pos (next-single-property-change start 'display nil limit)))
    (while
        (and pos (< pos limit)
             (let ((display-prop
                    (plist-get (text-properties-at pos) 'display)))
               (or (not display-prop)
                   (not (plist-get (cdr display-prop) :data)))))
      (setq pos (next-single-property-change pos 'display nil limit)))
    pos))
;; #+END_SRC

;; *** org specific

;; #+BEGIN_SRC emacs-lisp


;; org

(defun xxx-mw-org-attach-embedded-image-with-sha1-name (text-properties)
  "Save the image given in TEXT-PROPERTIES as org attachment.
The file gets the file name extension '.someimage'.

Hint (for redisplay the images in Emacs):
- Consider to add 'someimage' to `image-file-name-extensions'."
  (let ((display-data (cdr (plist-get text-properties 'display))))
    (when display-data
      (when-let ((data (plist-get display-data :data)))
        (let* ((extension (symbol-name (image-type-from-data data)))
               (basename (concat (sha1 data) "." extension))
               (org-attach-filename
                (concat (org-attach-dir t) "/" basename)))
          (unless (file-exists-p org-attach-filename)
            (with-temp-file org-attach-filename
              (setq buffer-file-coding-system 'binary)
              (set-buffer-multibyte nil)
              (insert data)))
          (org-attach-sync)
          org-attach-filename)))))

(defun xxx-org-attach-embedded-images-in-subtree ()
  "Save the displayed images as attachments and insert links.
This function is useful when images have been copied into the org
but the actual images are not available as file yet locally."
  (interactive)
  (save-excursion
    (let* ((before-first-heading (org-before-first-heading-p))
           (beg (if before-first-heading
                    (point-min)
                  (org-back-to-heading)
                  (point)))
           (end (if before-first-heading
                    (point-max)
                  (org-end-of-subtree)
                  (point))))
      (goto-char beg)
      (when (and (plist-get (text-properties-at (point)) 'display)
	         (plist-get (cdr (plist-get (text-properties-at (point)) 'display)) 'data))
        (let ((name (concat "\n[[" (mw-org-attach-embedded-image-with-sha1-name
                        (text-properties-at (point))) "]]")))
          (goto-char (next-property-change (point) nil end))
          (skip-chars-forward "]")
          (insert name)))
      (let ((pos (mw-next-property-display-data (point) end)))
        (while (< pos end)
          (goto-char pos)
          (let ((name (concat "\n[[" (mw-org-attach-embedded-image-with-sha1-name
                                    (text-properties-at (point))) "]]")))
            (goto-char (next-property-change (point) nil end))
            (skip-chars-forward "]")
            (insert name))
          (setq pos (mw-next-property-display-data (point) end)))))))
;; #+END_SRC

;; ** Jump to end of Orgee

;; Actually this functionality is already implemented but not as command.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun org-jump-to-end-of-subtree ()
  "Move to the end of the current subtree."
  (interactive)
  (org-end-of-subtree))
;; #+END_SRC

;; ** Count direct sub orgees
;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-org-count-subtrees ()
  "Print the count of the direct subtrees below point."
  (interactive)
  (message "[%d direct subtrees]" (mw-org--count-subtrees)))

(defun mw-org--count-subtrees ()
  "Count the direct subtrees below the current outline level."
  (let ((level (org-outline-level)))
    (save-excursion
      (outline-next-heading)
      (if (<= (org-outline-level) level)
          0
        (let ((count 0)
              (level-to-count (org-outline-level))
              pos)
          (while (progn
                   (setq pos (point))
                   (when  (eq level-to-count (org-outline-level))
                     (incf count))
                   (and (outline-next-heading)
                        (<= level-to-count (org-outline-level))
                        (< pos (point)))))
          count)))))
;; #+END_SRC

;; ** Show/Hide meta-info lines

;; E.g. hide src-block-delimiters.

;; I think there is an issue when using org-ellipsis: the org-ellipses
;; appears.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(let (ols)

  (defun mw-org-hide-meta-heading-info ()
    "Hide meta data following headings."
    (interactive)
    (org-show-all) ; expand all props before make invisible to avoid ellipses.
    (save-excursion
      (goto-char (point-min))
      (unless (org-at-heading-p) (outline-next-heading))
      (while (not (eobp))
        (let ((beg (1+ (progn (end-of-line) (point))))
              (end (1- (progn (org-end-of-meta-data t) (point)))))
          (when (< beg end)
            (push (make-overlay beg end) ols)
            (overlay-put (car ols) 'invisible t)))
        (when (not (org-at-heading-p))
          (outline-next-heading)))))

  (defun mw-org-hide-meta-info-lines ()
    "Hide parts of meta info.
E.g. hide all property sections."
    (interactive)
    (org-show-all)
    (save-excursion
      (goto-char (point-min))
      (let ((case-fold-search t))
        (while (re-search-forward
                (concat
                 "^\\( *scheduled:.*\\)" "\\|"
                 "^\\( *deadline:.*\\)" "\\|"
                 "^\\( *closed:.*\\)" "\\|"
                 ;; "^\\( *#\\+begin_src.*\\)" "\\|"
                 ;; "^\\( *#\\+end_src.*\\)" "\\|"
                 ;; "^\\( *#\\+begin_center.*\\)" "\\|"
                 ;; "^\\( *#\\+end_center.*\\)" "\\|"
                 ;; "^\\( *#\\+begin_example.*\\)" "\\|"
                 ;; "^\\( *#\\+end_example.*\\)" "\\|"
                 ;; "^\\( *#\\+begin_quote.*\\)" "\\|"
                 ;; "^\\( *#\\+end_quote.*\\)" "\\|"
                 ;; "^\\( *#\\+begin_verse.*\\)" "\\|"
                 ;; "^\\( *#\\+end_verse.*\\)" "\\|"
                 ;; "^\\( *#\\+results:\\)" "\\|"
                 "\\(^ *:properties:.*\\)\\(\n.*\\)*?\n\\( *:end:.*\\)" "\\|"
                 "\\(^ *:logbook:.*\\)\\(\n.*\\)*?\n\\( *:end:.*\\)"
                 )
                nil t)
          (push (make-overlay  (match-beginning 0) (1+ (match-end 0)))
                ols)
          (overlay-put (car ols) 'invisible t)))))

  (defun mw-org-show-meta-info-lines ()
    "Show meta info."
    (interactive)
    (mapc #'delete-overlay ols)
    (setq ols nil)))
;; #+END_SRC

;; ** View Subtree as top level
;; *** Code
;; #+BEGIN_SRC emacs-lisp
(let ((top-level-risen-indicator-char " ")
      overlays it-s-on)

  (defun mw-org-subtree-as-top-level-tree-establish ()
    "View Subtree as top level."
    (mw-org-subtree-as-top-level-tree-unravel)
    (unless (< (org-outline-level) 2)
      (let (first-level)
        (org-map-entries
         (lambda ()
           (let ((level (org-outline-level)))
             (unless first-level
               (setf first-level level))
             (push (make-overlay (1+ (point)) (+ (point) first-level)) overlays)
             (overlay-put (car overlays) 'invisible t)
             (unless (= first-level level)
               (push (make-overlay
                     (+ (point) first-level)
                     (+ 1 (point) first-level))
                    overlays)
               (overlay-put (car overlays) 'display top-level-risen-indicator-char))))
         t 'tree))
      (setq it-s-on t)))

  (defun mw-org-subtree-as-top-level-tree-unravel ()
    "Undo all decoration as top level."
    (dolist (ol overlays)
      (delete-overlay ol))
    (setf overlays nil)
    (setq it-s-on nil))

  (defun mw-org-subtree-as-top-level-tree-toggle ()
    (interactive)
    (if it-s-on
        (mw-org-subtree-as-top-level-tree-unravel)
      (mw-org-subtree-as-top-level-tree-establish)))

  (list
   #'mw-org-subtree-as-top-level-tree-toggle))
;; #+END_SRC

;; ** Narrow to level above

;; *** About

;; This function narrows to the level above.  When narrowing to subtree
;; is in effect, this function may widen one level.

;; IIRC Bernt Hansen also wrote such function.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-org-narrow-to-one-level-above ()
  "Narrow to one level above relative to point."
  (interactive)
  (widen)
  (let ((point-recall (point)))
    (unless (org-before-first-heading-p)
      (unless (outline-on-heading-p)
        (outline-previous-heading))
      (beginning-of-line)
      (if (= 1 (org-reduced-level (org-outline-level)))
          (widen)
        (assert (outline-on-heading-p))
        (assert (< 1 (org-reduced-level (org-outline-level))))
        (outline-up-heading 1)
        (org-narrow-to-subtree)))
    (goto-char point-recall)))
;; #+END_SRC

;; *** Rating

;; I use this function.

;; *** TODO Experiment with point movents with the command

;; 
;; ** Mark column in an org table

;; *** About

;; Mark the column containing point in an Org table.

;; There is also a function for marking a whole Org table.

;; *** Interface

;; | mw-org-table-mark-column | command |
;; | mw-org-table-mark-table  | command |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(require 'org)
(defun mw-org-table-mark-column ()
  "Mark the column containing point.

For tables with horizontal lines this function can fail."
  (interactive)
  (unless (org-at-table-p) (user-error "Not at a table"))
  (org-table-find-dataline)
  (org-table-check-inside-data-field)
  (let* ((col (org-table-current-column))
         (beg (org-table-begin))
	 (end (org-table-end)))
    (goto-char beg)
    (org-table-goto-column col)
    (re-search-backward "|" nil t)
    (push-mark)
    (goto-char (1- end))
    (org-table-goto-column (1+ col))
    (re-search-backward "|" nil t)
    (exchange-point-and-mark)
    (forward-char)))

(defun mw-org-table-mark-table ()
  "Mark the table at point."
  (interactive)
  (unless (org-at-table-p) (user-error "Not at a table"))
  (goto-char (1- (org-table-end)))
  (push-mark (point) t t)
  (goto-char (org-table-begin)))
;; #+END_SRC

;; **** Test

;; Play some with an org table.  E.g. start with the table below.

;; | a | b | a | b |
;; | c | d | c | d |
;; | e | f | e | f |
;; | g | h | g | h |
;; | i | j | i | j |

;; *** Rating

;; [2016-04-28 Thu 19:57] Useful occasionaly.

;; 
;; ** Rise Org Subtree

;; *** About

;; Pull out an orgee immediately before its parent.

;; Rise is slightly different from the classical promotion.

;; Rationale of this functionality: The risen subtree cannot
;; accidentially slurp siblings when risen.

;; **** Idea

;; Set speed-key H to org-hoist.

;; *** Interface

;; | ~org-rise~ | command |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun org-rise ()
  "Rise current subtree to one level higher if possible."
  (interactive)
  (cl-assert (not (org-before-first-heading-p)) "Before first heading")
  (cl-assert (< 1 (org-outline-level)) "At top outline level")
  (org-back-to-heading)
  (let ((subtree-start (point)))
    (outline-up-heading 1)
    (let ((insert-here (point)))
      (goto-char subtree-start)
      (if (eq last-command 'kill-region)
          (setq last-command 'ignore)) ; break the kill accumulation
      (org-cut-subtree)
      (goto-char insert-here)
      (org-paste-subtree nil org-subtree-clip))))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:34] Good.  Using it often.
;; 
;; ** Clear Checkboxes in a Subtree

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-org-clear-checkboxes-in-subtree ()
  (interactive)
  (save-restriction
    (save-excursion
      (require 'org)
      (org-narrow-to-subtree)
      (goto-char (point-min))
      (while (re-search-forward "\\[X\\]" (point-max) t)
        (replace-match "[ ]")))))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:48] Using this very occasionally.
;; 
;; ** Remove Prefix from Org Link Descriptions

;; *** About

;; Remove prefix 'file:' from Org link descriptions.

;; This is a convenience command.  Such prefixes often occur since this
;; is the default naming for file-links with Org AFAICT.  I want to get
;; easily rid of those prefixes since they block ffap, which I like to
;; use often.  Further I don't want to see this prefix.

;; **** Remark and todo

;; [2016-11-05 Sat 14:38] Maybe the new generation of Org links (v 9) can
;; solve this issue.  Need to invesigate the possibilities of the new
;; links.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-org-link-strip-decoration ()
  "Remove the \"file:\" prefix from an Org link."
  (interactive)
  (when (org-in-regexp org-bracket-link-regexp 1) ;; We do have a link at point.
    (let (remove desc link)
      (setq remove (list (match-beginning 0) (match-end 0)))
      (setq desc (when (match-end 3)
                   (if (string= "file:" (substring (match-string 3) 0 5))
                       (substring (match-string 3) 5)
                     (match-string 3))))
      (setq link (match-string 1))
      (apply #'delete-region remove)
      (goto-char (car remove))
      (insert (concat "[[" link "][" desc "]]")))))
;; #+END_SRC

;; #+BEGIN_SRC emacs-lisp
(defalias 'mw-org-link-remove-file-decoration 'mw-org-link-strip-decoration)
(make-obsolete
 'mw-org-link-remove-file-decoration
 "use the `mw-org-link-strip-decoration' instead." "[2016-09-23 Fri 11:01]")
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:48] Using this occasionally.

;; 
;; ** Pull out section of orgee 'Commentary'

;; #+BEGIN_SRC emacs-lisp

(defun mw-org-commentary-content-section (headline-title)
  "Return list of begin end of HEADLINE-TITLE section in current buffer."
  (let ((section
         (car
          (org-element-map
              (seq-filter
               (lambda (x) (string= headline-title (car (org-element-property :title x))))
               (org-element-map (org-element-parse-buffer) 'headline #'identity))
              'section #'identity))))
    (when section
      (list (org-element-property :contents-begin section)
            (org-element-property :contents-end section)))))


(defun mw-org-babel-insert-org-section-as-elisp-comments (headline-title)
  "Insert the section of orgee 'Commentary' as elisp comment.
This is thought to ease DRY for commentary for literate
programming."
  (let* ((s_e (mw-org-commentary-content-section headline-title))
         (start (car s_e))
         (end (cadr s_e))
         (buf (current-buffer)))
    (insert
     (with-temp-buffer
       (let ((tbuf (current-buffer)))
         (with-current-buffer buf
           (append-to-buffer tbuf start end)))
       (emacs-lisp-mode)
       (comment-region (point-min) (point-max))
       (buffer-string)))))

(defun mw-org-babel-insert-org-commentary-section-as-elisp-comments ()
    (interactive)
    (mw-org-babel-insert-org-section-as-elisp-comments "Commentary"))

(defun mw-org-babel-insert-org-license-section-as-elisp-comments ()
  (interactive)
  (mw-org-babel-insert-org-section-as-elisp-comments "License"))
;; #+END_SRC

;; #+BEGIN_SRC emacs-lisp
(defun org-agenda-property-action ()
  "Property action for the current headline in the agenda."
  (interactive)
  (org-agenda-check-no-diary)
  (let* ((hdmarker (or (org-get-at-bol 'org-hd-marker)
		       (org-agenda-error)))
	 (buffer (marker-buffer hdmarker))
	 (pos (marker-position hdmarker))
	 (inhibit-read-only t)
	 newhead)
    (org-with-remote-undo buffer
      (with-current-buffer buffer
	(widen)
	(goto-char pos)
	(org-show-context 'agenda)
	(call-interactively 'org-property-action)))))
;; #+END_SRC

;; 
;;; Tail:                                                             :noexport:

;; ** Provide Emacs lib

;; #+BEGIN_SRC emacs-lisp
(provide 'org-supplements)
;; #+END_SRC

;; 
;; ** Closing notes

;; # Local _DISABLED_ Variables:
;; # lentic-init: lentic-orgel-org-init
;; # End:

;;; org:-supplements.el ends here
