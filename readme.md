<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org1d318b2">1. Commentary</a>
<ul>
<li><a href="#org0aff442">1.1. What</a></li>
<li><a href="#org54b57f0">1.2. Install</a></li>
<li><a href="#orgfa4f19b">1.3. Usage</a></li>
</ul>
</li>
</ul>
</div>
</div>


<a id="org1d318b2"></a>

# Commentary


<a id="org0aff442"></a>

## What

This file contains a bunch of functions which might help in some
situations.  [\\![Build Status](<https://travis-ci.org/marcowahl/little-helpers.svg?branch=master>)](<https://travis-ci.org/marcowahl/little-helpers>)

No warranties.  Communication welcome.


<a id="org54b57f0"></a>

## Install

Just pick the functionality you like e.g. by cut and paste.
Typically each code-section is self contained.

See the code of the supplements at

-   Emacs [\\![little-helpers.md](<https://github.com/marcowahl/little-helpers/blob/master/little-helpers.md>)](<https://github.com/marcowahl/little-helpers/blob/master/little-helpers.md>)
-   Org [\\![org-supplements.md](<https://github.com/marcowahl/little-helpers/blob/master/org-supplements.md>)](<https://github.com/marcowahl/little-helpers/blob/master/org-supplements.md>)

To have **all** functions of file little-helpers.el available put file
little-helpers.el into Emacs' load-path.  E.g. add the lines

    (push "...path/to/this/file" load-path)
    (require 'little-helpers)

to your Emacs configuration.


<a id="orgfa4f19b"></a>

## Usage

Just pick a function, possibly bind it to a key and hopefully enjoy
the functionality.

You can use this file as elisp lib little-helpers.  (See [Install](#org54b57f0).)
