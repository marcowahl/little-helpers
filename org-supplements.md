<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgcc46dae">1. Commentary</a>
<ul>
<li><a href="#orgf71db55">1.1. Install</a></li>
<li><a href="#orgaf4d206">1.2. Dependencies</a></li>
</ul>
</li>
<li><a href="#org58e9552">2. Features</a>
<ul>
<li><a href="#org1267131">2.1. Count direct sub orgees</a></li>
<li><a href="#org557e9eb">2.2. Show/Hide src-block-delimiters</a></li>
<li><a href="#orge2c7671">2.3. View Orgee as top level</a></li>
<li><a href="#org74bd1bb">2.4. Narrow to level above</a></li>
<li><a href="#orgd6312da">2.5. Mark column in an org table</a></li>
<li><a href="#org335385b">2.6. Rise Org Subtree</a></li>
<li><a href="#orgf70fb42">2.7. Clear Checkboxes in a Subtree</a></li>
<li><a href="#orgf0f3ac2">2.8. Remove Prefix from Org Link Descriptions</a></li>
</ul>
</li>
</ul>
</div>
</div>



<a id="orgcc46dae"></a>

# Commentary

This file contains a bunch of supplements for Org mode.

-   Expect changes any time!

-   This file contains sections for certain functionality.
    -   Actually it's written in lentic style.


<a id="orgf71db55"></a>

## Install

    (push "...path/to/this/file" load-path)
    (require 'org-supplements.org)


<a id="orgaf4d206"></a>

## Dependencies

The Org package needs to be in the right place.



<a id="org58e9552"></a>

# Features




<a id="org1267131"></a>

## Count direct sub orgees


<a id="orgfed1b63"></a>

### Code

    (defun org-count-sub-orgees ()
      "Print the count of the direct subtrees below point."
      (interactive)
      (message "[%d direct suborgs]" (org--count-sub-orgs)))

    (defun org--count-sub-orgees ()
      "Count the direct subtrees below the current outline level."
      (let ((level (org-outline-level)))
        (save-excursion
          (outline-next-heading)
          (if (<= (org-outline-level) level)
              0
            (let ((count 0)
                  (level-to-count (org-outline-level))
                  pos)
              (while (progn
                       (setq pos (point))
                       (when  (eq level-to-count (org-outline-level))
                         (setq count (1+ count)))
                       (and (outline-next-heading)
                            (< pos (point)))))
              count)))))


<a id="org557e9eb"></a>

## Show/Hide src-block-delimiters


<a id="orga340840"></a>

### Code

    (let (ols)

      (defun org-babel-src-block-delimiters-hide ()
        "Hide src-block delimiters."
        (interactive)
        (save-excursion
          (goto-char (point-min))
          (let ((case-fold-search t))
            (while (re-search-forward "^\\( *#\\+begin_src.*\\)\\|\\( *#\\+end_src.*\\)" nil t)
              (push (make-overlay  (match-beginning 0) (1+ (match-end 0))) ols)
              (overlay-put (car ols) 'evaporate t)
              (overlay-put (car ols) 'invisible t)))))

      (defun org-babel-src-block-delimiters-show ()
        "Show src-block delimiters."
        (interactive)
        (mapc #'delete-overlay ols)
        (setq ols nil)))


<a id="orge2c7671"></a>

## View Orgee as top level


<a id="org4116904"></a>

### Code

    (let ((top-level-risen-indicator-char "⊃")
          overlays it-s-on)

      (defun org-orgee-as-top-level-tree-establish ()
        (interactive)
        (org-orgee-as-top-level-tree-unravel)
        (unless (< (org-outline-level) 2)
          (let (first-level)
            (org-map-entries
             (lambda ()
               (let ((level (org-outline-level)))
                 (unless first-level
                   (setf first-level level))
                 (push (make-overlay (1+ (point)) (+ (point) first-level)) overlays)
                 (overlay-put (car overlays) 'invisible t)
                 (push (make-overlay  (+ (point) first-level) (+ 1 (point) first-level)) overlays)
                 (overlay-put (car overlays) 'display top-level-risen-indicator-char)
                 ))
             t 'tree))))

      (defun org-orgee-as-top-level-tree-unravel ()
        (interactive)
        (dolist (ol overlays)
          (delete-overlay ol))
        (setf overlays nil)))


<a id="org74bd1bb"></a>

## Narrow to level above


<a id="orge24297e"></a>

### About

This function narrows to the level above.  When narrowing to subtree
is in effect, this function may widen one level.

IIRC Bernt Hansen also wrote such function.


<a id="org9674c82"></a>

### Code

    (defun mw-org-narrow-to-one-level-above ()
      "Narrow to one level above relative to point."
      (interactive)
      (widen)
      (let ((point-recall (point)))
        (unless (org-before-first-heading-p)
          (unless (outline-on-heading-p)
            (outline-previous-heading))
          (beginning-of-line)
          (if (= 1 (org-reduced-level (org-outline-level)))
              (widen)
            (assert (outline-on-heading-p))
            (assert (< 1 (org-reduced-level (org-outline-level))))
            (outline-up-heading 1)
            (org-narrow-to-subtree)))
        (goto-char point-recall)))


<a id="org037e139"></a>

### Rating

I use this function.


<a id="org7a6d757"></a>

### Experiment with point movents with the command




<a id="orgd6312da"></a>

## Mark column in an org table


<a id="org1c0ee28"></a>

### About

Mark the column containing point in an Org table.

There is also a function for marking a whole Org table.


<a id="org0fe1d24"></a>

### Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">mw-org-table-mark-column</td>
<td class="org-left">command</td>
</tr>


<tr>
<td class="org-left">mw-org-table-mark-table</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org42769da"></a>

### Code

    (require 'org)
    (defun mw-org-table-mark-column ()
      "Mark the column containing point.

    For tables with horizontal lines this function can fail."
      (interactive)
      (unless (org-at-table-p) (user-error "Not at a table"))
      (org-table-find-dataline)
      (org-table-check-inside-data-field)
      (let* ((col (org-table-current-column))
             (beg (org-table-begin))
    	 (end (org-table-end)))
        (goto-char beg)
        (org-table-goto-column col)
        (re-search-backward "|" nil t)
        (push-mark)
        (goto-char (1- end))
        (org-table-goto-column (1+ col))
        (re-search-backward "|" nil t)
        (exchange-point-and-mark)))

    (defun mw-org-table-mark-table ()
      "Mark the table at point."
      (interactive)
      (unless (org-at-table-p) (user-error "Not at a table"))
      (goto-char (1- (org-table-end)))
      (push-mark (point) t t)
      (goto-char (org-table-begin)))

1.  Test

    Play some with an org table.  E.g. start with the table below.

    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


    <colgroup>
    <col  class="org-left" />

    <col  class="org-left" />

    <col  class="org-left" />

    <col  class="org-left" />
    </colgroup>
    <tbody>
    <tr>
    <td class="org-left">a</td>
    <td class="org-left">b</td>
    <td class="org-left">a</td>
    <td class="org-left">b</td>
    </tr>


    <tr>
    <td class="org-left">c</td>
    <td class="org-left">d</td>
    <td class="org-left">c</td>
    <td class="org-left">d</td>
    </tr>


    <tr>
    <td class="org-left">e</td>
    <td class="org-left">f</td>
    <td class="org-left">e</td>
    <td class="org-left">f</td>
    </tr>


    <tr>
    <td class="org-left">g</td>
    <td class="org-left">h</td>
    <td class="org-left">g</td>
    <td class="org-left">h</td>
    </tr>


    <tr>
    <td class="org-left">i</td>
    <td class="org-left">j</td>
    <td class="org-left">i</td>
    <td class="org-left">j</td>
    </tr>
    </tbody>
    </table>


<a id="orgdabed78"></a>

### Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 19:57] </span></span> Useful occasionaly.




<a id="org335385b"></a>

## Rise Org Subtree


<a id="orgd1f20a6"></a>

### About

Pull out an orgee immediately before its parent.

Rise is slightly different from the classical promotion.

Rationale of this functionality: The risen subtree cannot
accidentially slurp siblings when risen.

1.  Idea

    Set speed-key H to org-rise.


<a id="orge586174"></a>

### Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`org-rise`</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org99c1358"></a>

### Code

    (defun org-rise ()
    "Rise current subtree to one level higher."
      (interactive)
      (let ((init-pos (point)))
        (org-copy-subtree)
        (outline-up-heading 1)
        (org-paste-subtree)
        (save-excursion
          (goto-char (+ init-pos (length org-subtree-clip)))
          (org-cut-subtree))))


<a id="orgdba992d"></a>

### Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:34] </span></span> Good.  Using it often.



<a id="orgf70fb42"></a>

## Clear Checkboxes in a Subtree


<a id="org3c188a1"></a>

### Code

    (defun mw-org-clear-checkboxes-in-subtree ()
      (interactive)
      (save-restriction
        (save-excursion
          (require 'org)
          (org-narrow-to-subtree)
          (goto-char (point-min))
          (while (re-search-forward "\\[X\\]" (point-max) t)
            (replace-match "[ ]")))))


<a id="orgc22bc75"></a>

### Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:48] </span></span> Using this very occasionally.



<a id="orgf0f3ac2"></a>

## Remove Prefix from Org Link Descriptions


<a id="org6857a5a"></a>

### About

Remove prefix 'file:' from Org link descriptions.

This is a convenience command.  Such prefixes often occur since this
is the default naming for file-links with Org AFAICT.  I want to get
easily rid of those prefixes since they block ffap, which I like to
use often.  Further I don't want to see this prefix.

1.  Remark and todo

    <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 14:38] </span></span> Maybe the new generation of Org links (v 9) can
    solve this issue.  Need to invesigate the possibilities of the new
    links.


<a id="org380fadc"></a>

### Code

    (defun mw-org-link-strip-decoration ()
      "Remove the \"file:\" prefix from an Org link."
      (interactive)
      (when (org-in-regexp org-bracket-link-regexp 1) ;; We do have a link at point.
        (let (remove desc link)
          (setq remove (list (match-beginning 0) (match-end 0)))
          (setq desc (when (match-end 3)
                       (if (string= "file:" (substring (match-string 3) 0 5))
                           (substring (match-string 3) 5)
                         (match-string 3))))
          (setq link (match-string 1))
          (apply #'delete-region remove)
          (goto-char (car remove))
          (insert (concat "[[" link "][" desc "]]")))))

    (defalias 'mw-org-link-remove-file-decoration 'mw-org-link-strip-decoration)
    (make-obsolete
     'mw-org-link-remove-file-decoration
     "use the `mw-org-link-strip-decoration' instead." "[2016-09-23 Fri 11:01]")


<a id="orgaf20f3d"></a>

### Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:48] </span></span> Using this occasionally.


