;;; test-little-helpers.el --- Tests for little-helpers.el

;;; Header:

;; Copyright 2014-2016 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl <marcowahlsoft@gmail.com>
;; Created: 2014
;; Keywords: convenience, helpers
;; URL: https://marcowahl.github.io/ https://github.com/marcowahl/little-helpers

;; This file is not part of Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; 
;;; Commentary:

;; ** What

;; This file contains a bunch of tests for functions in
;; little-helpers.el.

;; ** trigger the tests

;; - [optional] (ert-delete-all-tests)
;; - Evaluate (some) emacs lisp of this file e.g. an individual test or
;;   all the file (eval-buffer)
;; - Trigger the tests with
;;   - M-x ert
;;   - (ert t)

;; ** Lentic style

;; This emacs-lisp file started in lentic style.  See
;; https://github.com/phillord/lentic.

;; Wish: get back to lentic.

;; 
;;; Tests:

;; #+BEGIN_SRC emacs-lisp
(ert-deftest da8cdc95c077cd5b6fe366dbb286e4864da9b123 ()
  (should (= 0 (mw-day-of-week '(2018 9 23)))))

(ert-deftest 4d77471e767d846962de284cd8899f9b493da9de ()
  (should (= 1 (mw-day-of-week '(2018 9 24)))))

(ert-deftest 2953d4c1a37f66e4f92fa6a6ac17193b45acfd66 ()
  (should (= 2 (mw-day-of-week '(2018 9 25)))))

(ert-deftest db40aea7eac99ec6dcd2523fa77510b9f4e7c39e ()
  (should (= 3 (mw-day-of-week '(2018 9 26)))))

(ert-deftest d8664251bc3a2a8483077bee6dbbf7bcbac438e9 ()
  (should (= 4 (mw-day-of-week '(2018 9 27)))))

(ert-deftest 0f5612303663c27818032c20427633e3fe630e3a ()
  (should (= 5 (mw-day-of-week '(2018 9 28)))))

(ert-deftest b9f988f7a2cb23f4acbd6d154a9682193336827e ()
  (should (= 6 (mw-day-of-week '(2018 9 29)))))
;; #+END_SRC emacs-lisp


;; ** Umlaut Creation

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/mw-umlautify/conversions ()
  (with-temp-buffer
    (insert "123s")
    (mw-umlautify-before-point)
    (should
     (string= (buffer-substring (1- (point)) (point)) "ß")))
  (with-temp-buffer
    (insert "123a")
    (mw-umlautify-before-point)
    (should
     (string= (buffer-substring (1- (point)) (point)) "ä"))))

(ert-deftest test/mw-umlautify/no-conversions ()
  (with-temp-buffer
    (insert "123x")
    (mw-umlautify-before-point)
    (should
     (string= (buffer-substring (1- (point)) (point)) "x")))
  (with-temp-buffer
    (insert "123ä")
    (mw-umlautify-before-point)
    (should
     (string= (buffer-substring (1- (point)) (point)) "ä"))))
;; #+END_SRC
;; 
;; ** Freeze Emacs Time

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/freeze-time/1 ()
  (freeze-time-to "2016-06-06")
  (should (string= "2016-06-06" (format-time-string "%Y-%m-%d" (current-time))))
  (freeze-time-unfreeze))

(ert-deftest test/freeze-time/2 ()
  (freeze-time-to "2216-06-01")
  (should (string= "2216-06-01" (format-time-string "%Y-%m-%d" (current-time))))
  (freeze-time-unfreeze))
;; #+END_SRC

;; 
;; ** String transformation

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/increment-empty-string ()
  (setq mw-fill-with-zeros t)
  (should (string-equal nil (mw-increment-last-number ""))))

(ert-deftest test/increment-string-without-number ()
  (setq mw-fill-with-zeros t)
  (should (string-equal nil (mw-increment-last-number "foo"))))

(ert-deftest test/increment-pure-number ()
  (setq mw-fill-with-zeros t)
  (should (string-equal "2" (mw-increment-last-number "1"))))

(ert-deftest test/increment-number-with-leading-zeros ()
  (setq mw-fill-with-zeros t)
  (should (string-equal "007" (mw-increment-last-number "006"))))

(ert-deftest test/increment-embedded-number-1 ()
  (setq mw-fill-with-zeros t)
  (should
   (string-equal
    "baz-42-foo007bar"
    (mw-increment-last-number "baz-42-foo006bar"))))

(ert-deftest test/decr-embedded-number-1 ()
  (setq mw-fill-with-zeros t)
  (should
   (string-equal
    "baz-42-foo007bar"
    (mw-decrement-last-number "baz-42-foo008bar"))))

(ert-deftest test/operate-on-last-number-increment ()
  (setq mw-fill-with-zeros t)
  (should
   (string-equal
    "baz-42-foo007bar"
    (mw-operate-on-last-number "baz-42-foo006bar" '1+))))

(ert-deftest test/operate-on-last-number-some-fun ()
  (setq mw-fill-with-zeros t)
  (should
   (string-equal
    "baz-42-foo007bar"
    (mw-operate-on-last-number "baz-42-foo042bar" (lambda (x) (/ x 6))))))

(ert-deftest test/operate-on-last-number-new-number-covers-leading-zeros ()
  (setq mw-fill-with-zeros t)
  (should
   (string-equal
    "baz-42-foo4242bar"
    (mw-operate-on-last-number "baz-42-foo042bar" (lambda (x) 4242)))))

(ert-deftest test/operate-on-last-number-new-number-has-less-digits ()
  (setq mw-fill-with-zeros t)
  (should
   (string-equal
    "baz-42-foo0000bar"
    (mw-operate-on-last-number "baz-42-foo1234bar" (lambda (x) 0)))))
;; #+END_SRC

;; 
;; *** Without zero-fill

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/operate-on-last-number-new-number-has-less-digits-no-fill ()
  (setq mw-fill-with-zeros nil)
  (should
   (string-equal
    "baz-42-foo0bar"
    (mw-operate-on-last-number "baz-42-foo1234bar" (lambda (x) 0)))))
;; #+END_SRC
;; 
;; ** Line Duplication

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/mw-duplicate-line-1-just-one-line-in-buffer ()
  (with-temp-buffer
    (let ((text "a line"))
      (goto-char (point-min))
      (insert text "\n")
      (goto-char (point-min))
      (mw-duplicate-line)
      (goto-char (point-min))
      (should (search-forward (concat text "\n" text))))))
;; #+END_SRC

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/mw-duplicate-line-2-line-in-the-middle ()
  (with-temp-buffer
    (let ((text "a line"))
      (insert "foobarbaz foobarbaz\n")
      (insert text)
      (insert "\nfoobarbaz foobarbaz")
      (goto-char (point-min))
      (re-search-forward (concat "^" text))
      (mw-duplicate-line)
      (goto-char (point-min))
      (should (search-forward (concat text "\n" text))))))
;; #+END_SRC
;; 
;; ** Comma-Dot Toggle

;; #+BEGIN_SRC emacs-lisp
(defmacro trans-comma-dot (in expect)
  `(should (equal ,expect (mw-switch-comma-and-dot-in-string ,in))))

(ert-deftest test/mw-switch-comma-and-dot-in-string-0 ()
  (trans-comma-dot "" ""))
(ert-deftest test/mw-switch-comma-and-dot-in-string-00 ()
  (trans-comma-dot "foo" "foo"))
(ert-deftest test/mw-switch-comma-and-dot-in-string-1 ()
  (trans-comma-dot "1.00" "1,00"))
(ert-deftest test/mw-switch-comma-and-dot-in-string-2 ()
  (trans-comma-dot "1,00" "1.00"))
(ert-deftest test/mw-switch-comma-and-dot-in-string-3 ()
  (trans-comma-dot "1,000.00" "1.000,00"))
(ert-deftest test/mw-switch-comma-and-dot-in-string-4 ()
  (trans-comma-dot "1.000,00" "1,000.00"))
(ert-deftest test/mw-switch-comma-and-dot-in-string-5 ()
  (trans-comma-dot "1,000,000.00" "1.000.000,00"))
(ert-deftest test/mw-switch-comma-and-dot-in-string-6 ()
  (trans-comma-dot "1.000.000,00" "1,000,000.00"))
;; #+END_SRC

;; 
;; ** Jump to line and recenter to top

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/mw-recenter-jump-to-top-1 ()
  "Test if the function is available."
  (fboundp 'mw-recenter-jump-to-top))
;; #+END_SRC

;; 
;; ** Remove Prefix from Org Link Descriptions

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/mw-org-link-remove-file-decoration-1 ()
  (should (string-equal
           "[[file:/tmp][/tmp]]"
           (with-temp-buffer
             (insert "[[file:/tmp][file:/tmp]]")
             (mw-org-link-remove-file-decoration)
             (buffer-string)))))

(ert-deftest test/mw-org-link-remove-file-decoration-2 ()
  (should (string-equal
           "[[file:/tmp][tmp]]"
           (with-temp-buffer
             (insert "[[file:/tmp][file:tmp]]")
             (mw-org-link-remove-file-decoration)
             (buffer-string)))))

(ert-deftest test/mw-org-link-remove-file-decoration-3 ()
  (should (string-equal
           "[[bla bla bla][gaa tmp]]"
           (with-temp-buffer
             (insert  "[[bla bla bla][gaa tmp]]")
             (mw-org-link-remove-file-decoration)
             (buffer-string)))))
;; #+END_SRC
;; 
;; ** Clear Checkboxes in an Org Subtree

;; #+BEGIN_SRC emacs-lisp
(ert-deftest test/mw-org-clear-checkboxes-in-subtree ()
  (should
   (string-equal
    "* foo\n[X]\n** bar\n[ ] baz"
    (with-temp-buffer
      (insert "* foo\n[X]\n** bar\n[X] baz")
      (goto-char (point-min))
      (search-forward-regexp "\\*\\*")
      (mw-org-clear-checkboxes-in-subtree)
      (buffer-string)))))
;; #+END_SRC




;; 

;;; Tail:

;; # Local _DISABLED_ Variables:
;; # lentic-init: lentic-orgel-org-init
;; # eval: (git-auto-commit-mode)
;; # End:

;;; test-little-helpers.el ends here
