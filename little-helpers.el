;;; little-helpers.el --- Little helpers for Emacs  -*- lexical-binding: t -*-

;; #+STARTUP: oddeven
;; #+options: toc:2

;;; Header:                                                           :noexport:

;; Copyright 2014-2016 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl <marcowahlsoft@gmail.com>
;; Created: 2014
;; Version: 0.1
;; Keywords: convenience, helpers
;; URL: https://marcowahl.github.io/ https://github.com/marcowahl/little-helpers

;; This file is not part of Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; ** What

;; This file contains a bunch of functions which might help in some
;; situations.  [![Build Status](https://travis-ci.org/marcowahl/little-helpers.svg?branch=master)](https://travis-ci.org/marcowahl/little-helpers)

;; No warranties.  Communication welcome.

;; ** Install
;; :PROPERTIES:
;; :ID:       56c08719-aa3e-4970-ac88-6f7bddd63158
;; :END:

;; Just pick the functionality you like e.g. by cut and paste.
;; Typically each code-section is self contained.

;; See the code of the supplements at
;;  - Emacs [![little-helpers.md](https://github.com/marcowahl/little-helpers/blob/master/little-helpers.md)](https://github.com/marcowahl/little-helpers/blob/master/little-helpers.md)
;;  - Org [![org-supplements.md](https://github.com/marcowahl/little-helpers/blob/master/org-supplements.md)](https://github.com/marcowahl/little-helpers/blob/master/org-supplements.md)

;; To have *all* functions of file little-helpers.el available put file
;; little-helpers.el into Emacs' load-path.  E.g. add the lines

;; #+BEGIN_SRC text
;; (push "...path/to/this/file" load-path)
;; (require 'little-helpers)
;; #+END_SRC

;; to your Emacs configuration.

;; ** Usage

;; Just pick a function, possibly bind it to a key and hopefully enjoy
;; the functionality.

;; You can use this file as elisp lib little-helpers.  (See [[id:56c08719-aa3e-4970-ac88-6f7bddd63158][Install]].)

;; ** Tests                             :noexport:

;; *** What Tests?

;; There are some tests in a file =test-little-helpers.el=.  They
;; assure some functionality.

;; *** Automatic Tests on a Server

;; The tests are triggered automatically on a travis server after each
;; commit to github.  (This is the case at [2016-04-28 Thu 22:13])

;; *** Trigger the Tests Locally

;; **** Step by Step

;; You can evaluate the emacs lisp of the file containing the tests
;; e.g. with M-x eval-buffer.  Trigger the tests with M-x ert.

;; **** More Automation

;; The following code runs the tests.

;; (progn
;;   (find-file "test-little-helpers.el")
;;   (eval-buffer)
;;   (ert t))

;; ** Generate Documentation                                   :noexport:

;; Ideally the source files contains all documentation.  Generation of
;; documentation means to extract and transform the right parts of
;; the source files.

;; ./readme.md is the main docu document.  Currently
;; [2016-11-05 Sat 16:11] the generation is pseude automatic.  See file
;; [[file:compile-docu.org][./compile-docu.org]] for an ansatz how concrete documentation could be
;; generated.

;; ** Lentic style                                :noexport:

;; This emacs-lisp file started in lentic style.  See
;; https://github.com/phillord/lentic.

;; Wish: get back to lentic.

;;; Code:                                                             :noexport:

;; ** Replace according a map                                              :lab:

;; #+begin_src emacs-lisp
;; (defun mw-replace-regexp-several (replacements)
;;   "replace-regexp for each item in REPLACEMENTS.

;; E.g. '((\"-Apr-\" . \"-04-\") (\"-Aug-\" . \"-08-\"))"
;;   (let ((p (point)))
;;     (mapc (lambda (x) (goto-char p) (replace-regexp (car x) (cdr x)))
;;           replacements)))
;; #+end_src

;; *** test

;; watch out!  narrow to subtree before testing!

;; (mw-replace-regexp-several '(("-Apr-" . "-04-") ("-Aug-" . "-08-")))

;; -Apr-2017
;; -May-2017
;; -Aug-2017

;; ** Kill all clones                                                      :lab:

;; With the setting

;; (setq clone-indirect-buffer-hook
;;       (lambda ()
;;         (make-local-variable 'is-indirect-clone-p)
;;         (setq is-indirect-clone-p t)))

;; in the configuration every clone gets marked.  This allows to kill
;; the clones easily.

;; #+BEGIN_SRC emacs-lisp
(defun mw-kill-all-clones ()
  "Kill all buffers which bear symbol `is-indirect-clone-p'."
  (interactive)
  (mapc #'kill-buffer
        (seq-filter (lambda (buf)
                      (set-buffer buf)
                      (boundp 'is-indirect-clone-p))
                    (buffer-list))))
;; #+END_SRC

;; ** get day of the week

;; this is for the date given in the "natural" form like (2018 9 23).

;; TODO: provide a comfortable interactive version.

;; #+BEGIN_SRC emacs-lisp
(defun mw-day-of-week (date)
  "Return day of week as number 0 means Sunday.
DATE is a list like (2018 9 23) (Year Month Day)."
  (calendar-day-of-week (append (cdr date) (list (car date)))))
;; #+END_SRC

;; ** Line duplication

;; *** About

;; Just duplicate the line containing the cursor.

;; To bind key chord 'yy' to =mw-duplicate-line= might be a reliable
;; choice.

;; [2015-10-06 Tue 23:48] I use this function a lot.

;; *** Interface

;; | =mw-duplicate-line= | command |
;; | =mw-duplicate-line-as-term= | command |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-duplicate-line ()
  "Duplicate the line containing point."
  (interactive)
  (let ((line (buffer-substring
               (point-at-bol) (point-at-eol))))
    (forward-line)
    (insert line "\n")
    (forward-line -1)))

(defun mw-duplicate-line-as-term ()
  "Append equal sign and newline and duplicate the line containing point."
  (interactive)
  (let ((line (buffer-substring
               (point-at-bol) (point-at-eol))))
    (end-of-line)
    (just-one-space)
    (insert "=\n\n" line)
    (newline)
    (forward-line -1)))
;; #+END_SRC

;; *** Links

;; I can easily imagine that there are lots of programs out there which
;; do the line duplication already.

;; Consider also avy-copy-line of Emacs package avy.  Which has a
;; different touch.

;; [[http://melpa.org/#/key-chord]]

;; *** Rating

;; [2016-04-28 Thu 19:54] Useful.

;; ** save text property images in files

;; #+BEGIN_SRC emacs-lisp
(defvar image-to-file-sha1-hashed-directory "~/sha1-hashed/"
  "Directory for files named with their hash for .")
;; #+END_SRC

;; Save images given as data in text-properties to hash directory.

;; #+BEGIN_SRC emacs-lisp

(defun save-text-property-image-to-hashed-name (text-properties)
  "Write image given inline in TEXT-PROPERTIES to hash directory."
  (when-let* ((display-data (cdr (plist-get text-properties 'display)))
              (data (plist-get display-data :data)))
    (let ((filename
           (concat image-to-file-sha1-hashed-directory (sha1 data) ".someimage")))
      (if (file-exists-p filename)
          (message "Filename %s exists.  Writing nothing." filename)
        (with-temp-file filename
          (setq buffer-file-coding-system 'binary)
          (set-buffer-multibyte nil)
          (insert data))
        (message "Written %s" filename)))))

(defun save-text-property-images-in-region-to-hashed-files (beg end)
  "Copy text-property image data to files in region BEG END.
The files get the sha1 hash of their content as name."
  (interactive "r")
  ;; I guess this should be refined.  todo: check out the
  ;; text-properties world!
  (when (plist-get (text-properties-at beg) 'display)
    (save-text-property-image-to-hashed-name (text-properties-at beg)))
  (let ((pos (next-property-display-data beg end)))
    (while (< pos end)
      (save-text-property-image-to-hashed-name (text-properties-at pos))
      (setq pos (next-property-display-data pos end)))))
;; #+END_SRC

;; ** uniq

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun uniq (start end)
  "Drop directly following identical lines in in region.

This is like the unix uniq command."
  (interactive "r")
  (shell-command-on-region start end "uniq" nil t))
;; #+END_SRC

;; ** Operate on Numbers in String
;; *** About

;; Functions for operating on the last number in a string.

;; *** Interface

;; | name                        | arguments  | what     |
;; |-----------------------------+------------+----------|
;; | =mw-increment-last-number=  | string     | defun    |
;; | =mw-decrement-last-number=  | string     | defun    |
;; | =mw-operate-on-last-number= | string fun | defun    |
;; | =mw-fill-with-zeros=        |            | variable |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defvar mw-fill-with-zeros t
  "nil means to neither introduce nor respect given leading zeros
  in numbers for the string operations.  Non-nil means to keep
  the width for a given number by using the respective number of
  leading zeros.")

;; #+END_SRC
;; #+BEGIN_SRC emacs-lisp
(defun mw-increment-last-number (string)
  "Increase the last number in STRING by one."
  (mw-operate-on-last-number string '1+))

(defun mw-decrement-last-number (string)
  "Decrease the last number in STRING by one."
  (mw-operate-on-last-number string '1-))

(defun mw-operate-on-last-number (string fun)
  "Apply FUN on the last number in STRING."
  (when (string-match "\\([[:digit:]]+\\)[^[:digit:]]*$" string)
    (let* ((number-string (match-string 1 string))
	   (operated-number-string
	    (number-to-string (funcall fun (string-to-number number-string)))))
      (replace-match
       (concat
        (if mw-fill-with-zeros
            (let ((length-diff
                   (- (length number-string)
                      (length operated-number-string))))
              (if (< 0 length-diff)
                  (make-string length-diff ?0)
                ""))
          "")
        operated-number-string)
       nil nil string 1))))
;; #+END_SRC

;; *** Links

;; [[http://www.emacswiki.org/emacs/IncrementNumber]] (discovered
;; [2015-06-17 Wed 19:27])

;; *** Rating

;; [2016-04-28 Thu 19:55] Not sure but I think this function is used somewhere.

;; ** visible-mode control for ediff

;; *** About

;; Enable visible-mode for the buffers in a ediff session.  This can be
;; helpful e.g. for org-mode buffers which often have a lot of invisible
;; parts.

;; *** Interface

;; | =mw-ediff-set-visible-mode-in-ediff-buffers= | command |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-ediff-set-visible-mode-in-ediff-buffers ()
"Set `visible-mode' in all buffers of a ediff session.
Only functional in an ediff control buffer."
  (interactive)
  (ediff-barf-if-not-control-buffer)
  (message "ediff buffers set to visible-mode to reveal hidden parts.
M-x visible-mode toggles visible-mode for a buffer.")
  (when ediff-buffer-A (save-excursion (set-buffer ediff-buffer-A)
                                       (visible-mode)))
  (when ediff-buffer-B (save-excursion (set-buffer ediff-buffer-B)
                                       (visible-mode)))
  (when ediff-buffer-C (save-excursion (set-buffer ediff-buffer-C)
                                       (visible-mode))))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 19:56] Useful but using it very rare.

;; *** Rest

;; **** TODO Reset visible-mode on quitting ediff

;; ** Switch Comma and Dot

;; *** About

;; Sometimes it helps to have a tool to change 5.000,00 to 5,000.00
;; quickly.  In Germany they often use 5.000,00 while in the US they use
;; 5,000.00.

;; Concretely this has been useful for bringing some numbers into the
;; right format for the ledger program.

;; *** Interface

;; | =mw-switch-comma-and-dot-in-region= | command |
;; | =mw-switch-comma-and-dot-in-string= | defun   |

;; *** Code

;; Switch comma and dot in a string.

;; #+BEGIN_SRC emacs-lisp
(defun mw-switch-comma-and-dot-in-string (string-with-number)
  (with-temp-buffer
    (insert string-with-number)
    (goto-char (point-min))
    (while (search-forward-regexp "\[.,\]" nil t)
      (let ((insert-char (if (equal ?. (char-before)) ?, ?.)))
        (delete-char -1)
        (insert insert-char)))
    (buffer-string)))
;; #+END_SRC

;; Switch comma and dot in a region.

;; #+BEGIN_SRC emacs-lisp
(defun mw-switch-comma-and-dot-in-region (beg end)
  "Replace . with , and vice versa in region."
  (interactive "r")
  (let ((converted (mw-switch-comma-and-dot-in-string (buffer-substring beg end))))
    (delete-region beg end)
    (goto-char beg)
    (insert converted)))

(defalias 'mw-region-switch-comma-and-dot 'mw-switch-comma-and-dot-in-region)
;; #+END_SRC

;; *** Links

;; http://www.ledger-cli.org/index.html

;; *** Rating

;; [2016-04-28 Thu 19:56] Useful but using it very rare.

;; ** Jump to line and recenter to top

;; *** About

;; This is a realization to do the jump to a line and recenter that line
;; to the top of the window in one step.

;; *** Interface

;; | mw-recenter-jump-to-top | command |

;; *** Dependencies

;; | avy |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-recenter-jump-to-top ()
  (interactive)
  (advice-add 'avy-goto-line :after (lambda () (recenter 0)))
  (avy-goto-line)
  (advice-remove 'avy-goto-line (lambda () (recenter 0))))
;; #+END_SRC

;; *** Links

;; https://github.com/winterTTr/ace-jump-mode/
;; https://github.com/abo-abo/avy

;; *** Rating

;; [2016-04-28 Thu 22:30] Using this occasionally.

;; ** Save Buffer Encrypted for Certain Key                                :lab:
;; :PROPERTIES:
;; :ID:       06d152d1-9c9f-4508-8d8e-e562f3c1b3d2
;; :END:

;; *** About

;; When using the gpg suffix Emacs asks for a key or keys to encrypt for.
;; For my personal secrets I want the file encryted for me.  (Yes, I have
;; something to hide.)

;; This is an attempt to simplify the encryption process in this case.

;; *** Interface

;; | =mw-save-buffer-encrypted-for-me= | command |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-save-buffer-encrypted-for-me ()
  "Set certain 'encrypt to', `save-buffer', restore 'encrypt to'.
This function has an effect when saving gpg files and is thought
as convenience for me.

Hint: What about defadvice?  And further improvements?  Ideal: No
disturbance when saving at all."
  (interactive)
  (let (recent-epa-file-encrypt-to epa-file-encrypt-to)
    (setq  epa-file-encrypt-to "49010A040A3AE6F2")
    (save-buffer)
    (setq  epa-file-encrypt-to recent-epa-file-encrypt-to)))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:31] Never using this.

;; ** Mark first key in epa key list buffer                                :lab:

;; *** About

;; Mark the next key in a epa key list buffer.  This is particularly
;; useful for marking the first key in the list.

;; *** Interface

;; | mw-epa-mark-next-key | command |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-epa-mark-next-key ()
  "Mark next `epa-key'.
This defun is for working in a epa-key-listing buffer."
  (interactive)
  (let ((inhibit-read-only t)
	buffer-read-only
	properties)
    (goto-char (point-min))
    (goto-char (next-char-property-change (1+ (point))))
    (while (not (get-text-property (point) 'epa-key))
      (goto-char (next-char-property-change (1+ (point))))
      (if (eq (point) (point-max))
          (error "No key")))
    (setq properties (text-properties-at (point)))
    (delete-char 1)
    (insert "*")
    (set-text-properties (1- (point)) (point) properties)))
;; #+END_SRC

;; *** Links

;; [[id:06d152d1-9c9f-4508-8d8e-e562f3c1b3d2][Save Buffer Encrypted for Certain Key]]

;; *** Rating

;; [2016-04-28 Thu 22:31] Never using this.

;; ** Ephemeral decoration

;; *** About

;; Decorate buffer with various information but just ephemerally.

;; *** Interface

;; | mw-ephemeral-rectangle-enumerate-lines-sit   | command |
;; | mw-ephemeral-rectangle-enumerate-lines-timer | command |

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(let (overlays)

  (defun mw-ephemeral-delete-overlays ()
    "Delete all overlays."
    (mapc #'delete-overlay overlays)
    (setq overlays nil))

  (defun mw-ephemeral-rectangle-enumerate-lines-sit (beg end)
    "Enumerate the region lines in the column of BEG.
The enumeration disappears on any keypress or after a few
seconds.  Argument END is the other end of the region."
    (interactive "r")
    (save-excursion
      (let ((col (- (progn (goto-char beg) (point))
                    (progn (beginning-of-line) (point))))
            (ctr 1)
            (fill-spaces-number 0))
        (goto-char beg)
        (while
            (progn
              (push (make-overlay (point) (point))
                    overlays)
              (overlay-put
               (car overlays)
               'after-string (format "%s %2d " (make-string fill-spaces-number 32) ctr))
              (setq ctr (1+ ctr)
                    fill-spaces-number 0)
              (forward-line)
              (let ((linewidth (- (progn (end-of-line) (point))
                                  (progn (beginning-of-line) (point)))))
                (if (< col linewidth)
                    (forward-char col)
                  (end-of-line)
                  (setq fill-spaces-number (- col linewidth))))
              (< (point) end)))))
    (sit-for 9)
    (mw-ephemeral-delete-overlays))

  (defun mw-ephemeral-rectangle-enumerate-lines-timer (beg end)
    "Enumerate the region lines in the column of BEG.
The enumeration disappears on any keypress or after a few
seconds.  Argument END is the other end of the region."
    (interactive "r")
    (save-excursion
      (let ((col (- (progn (goto-char beg) (point))
                    (progn (beginning-of-line) (point))))
            (ctr 1)
            (fill-spaces-number 0))
        (goto-char beg)
        (while
            (progn
              (push (make-overlay (point) (point))
                    overlays)
              (overlay-put
               (car overlays)
               'after-string (format "%s %2d " (make-string fill-spaces-number 32) ctr))
              (setq ctr (1+ ctr)
                    fill-spaces-number 0)
              (forward-line)
              (let ((linewidth (- (progn (end-of-line) (point))
                                  (progn (beginning-of-line) (point)))))
                (if (< col linewidth)
                    (forward-char col)
                  (end-of-line)
                  (setq fill-spaces-number (- col linewidth))))
              (< (point) end)))))
    (run-with-timer 9 nil #'mw-ephemeral-delete-overlays))

  (defun mw-ephemeral-enumerate-visual-lines ()
    "Enumerate the visual lines in the window.
The numbering disappears when the user does soething in the
window or after some time.

Check the term visual line in case you are uncertain.

This enumeration can help to find a prefix argument to use
command `move-to-window-line'."
    (interactive)
    (let ((n 0)
          (lnmax-pos (save-excursion (move-to-window-line -2)))
          (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
      (save-excursion
        (while (and (<= (move-to-window-line n) lnmax-pos)
                    (< n line-number-enumerate-max))
          (push (make-overlay (point) (progn (end-of-visual-line) (point))) overlays)
          (overlay-put
           (car overlays) 'before-string
           (format "%3d " (1+ n)))
          (setq n (1+ n))))
      (sit-for 23)
      (mw-ephemeral-delete-overlays)))

  (defun mw-ephemeral-enumerate-visual-lines-in-region (beg end)
    "Enumerate the visual lines down starting with the line containing point."
    (interactive "r")
    (let* ((point-on-top-window-line
            (save-excursion
              (move-to-window-line 0)
              (point)))
           (point-on-bottom-window-line
            (save-excursion
              (move-to-window-line -1)
              (point)))
           (top-line
            (save-excursion
              (goto-char (max beg point-on-top-window-line))
              (mw-visual-line-number-with-point)))
           (bottom-line
            (save-excursion
              (goto-char (min end point-on-bottom-window-line))
              (mw-visual-line-number-with-point))))
      (save-excursion
        (move-to-window-line top-line)
        (push (make-overlay (point) (point)) overlays)
        (overlay-put
         (car overlays) 'after-string
         (format "%3d " 1))
        (let ((n 2))
          (while (<= n (1+ (- bottom-line top-line)))
            (move-to-window-line (1- (+ n top-line)))
            (push (make-overlay (point) (point)) overlays)
            (overlay-put
             (car overlays) 'after-string
             (format "%3d " n))
            (setq n (1+ n))))))
    (sit-for 23)
    (mw-ephemeral-delete-overlays))

  (defun mw-ephemeral-enumerate-visual-lines-down-from-point ()
    "Enumerate the visual lines down starting with the line containing point."
    (interactive)
    (let ((n 0)
          (line-number-with-point (mw-visual-line-number-with-point))
          (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
      (save-excursion
        (move-to-window-line (+ n line-number-with-point))
        (while (<= (+ line-number-with-point n) line-number-enumerate-max)
          (push (make-overlay (point) (point)) overlays)
          (overlay-put
           (car overlays) 'after-string
           (format "%3d " (1+ n)))
          (setq n (1+ n))
          (move-to-window-line (+ n line-number-with-point))))
      (sit-for 23)
      (mw-ephemeral-delete-overlays)))

  (defun mw-ephemeral-enumerate-visual-lines-relative-to-point ()
    "Enumerate the visual lines in the window relative to point."
    (interactive)
    (let ((n 0)
          (lnwp (mw-visual-line-number-with-point))
          (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
      (save-excursion
        (while (not (= line-number-enumerate-max (move-to-window-line n)))
          (push (make-overlay (point) (point)) overlays)
          (overlay-put
           (car overlays) 'after-string
           (format "%3d " (- n lnwp)))
          (setq n (1+ n)))
        (when (not (= (point-max) (progn (move-to-window-line -1) (point))))
          (push (make-overlay (point) (point)) overlays)
          (overlay-put
           (car overlays) 'after-string
           (format "%3d " (- n lnwp)))))
      (sit-for 23)
      (mw-ephemeral-delete-overlays)))

  (defun mw-ephemeral-enumerate-visual-lines-dist-to-border ()
    "Enumerate the visible lines in the window with distance to
borders.

This enumeration can help to find a prefix argument to use
command `move-to-window-line'.
"
    (interactive)
    (let ((n 0)
          (window-lines (mw-count-window-lines))
          (half-window-lines (/ (mw-count-window-lines) 2)))
      (while (<= n half-window-lines)
        (save-excursion
          (move-to-window-line n)
          (push (make-overlay (point) (point)) overlays)
          (overlay-put
           (car overlays) 'after-string
           (format "%3d " n)))
        (let ((m (- (1+ n))))
          (if (or (< n half-window-lines)
                  (oddp window-lines))
              (save-excursion
                (move-to-window-line m)
                (push (make-overlay (point) (point)) overlays)
                (overlay-put
                 (car overlays) 'after-string
                 (format "%3d " m)))))
        (setq n (1+ n)))
      (sit-for 23)
      (mw-ephemeral-delete-overlays))))
;; #+END_SRC

;; *** Rest

;; ** Print numbers for visual lines                                       :lab:
;; :PROPERTIES:
;; :ID:       20cd5656-b0e4-44c3-bdb7-02ae855098df
;; :END:

;; *** About

;; Variations of counting the lines as they appear in an Emacs window.

;; This is different from counting the lines in a file or in a buffer.

;; *** Interface

;; | mw-enumerate-visual-lines                   | command |
;; | mw-enumerate-visual-lines-relative-to-point | command |
;; | mw-enumerate-visual-lines-dist-to-border    | command |
;; | mw-count-window-lines                       | defun   |
;; | mw-visual-line-number-with-point            | defun   |

;; *** Code


;; #+BEGIN_SRC emacs-lisp
(defun mw-visual-line-number-with-point ()
  ;; Note: Think about speed up by exponential seach.
  (let ((pt (save-excursion (forward-line 0) (point)))
        (n 0))
    (save-excursion
      (while (< (progn
                   (move-to-window-line n)
                   (point))
                 pt)
        (setq n (1+ n))))
    n))
;; #+END_SRC

;; #+BEGIN_SRC emacs-lisp
(defun mw-ephermal-count-window-lines ()
  "Count the lines of the window."
  (let ((pt (save-excursion (move-to-window-line -1) (point)))
        (n 0))
    (save-excursion
      (while (< (progn
                   (move-to-window-line n)
                   (point))
                 pt)
        (setq n (1+ n))))
    n))
;; #+END_SRC

;; *** Links

;; linum-mode

;; *** Rating

;; [2016-04-28 Thu 22:31] Using this occasionally.

;; ** Split window below point

;; *** Code

;; #+begin_src emacs-lisp
(defun mw-visual-line-number ()
  "Visual line number of the line containing point."
  (save-excursion
    (let ((end-point (progn (beginning-of-visual-line) (point)))
          (count 1)
          (line-move-visual t))
      (move-to-window-line (1- count))
      (while (< (point) end-point)
        (incf count)
        (next-line))
      count)))

(defun mw-split-window-vertically-at-point ()
  "Split window vertically at point.
The top window stands still.

This has been useful for screenshots which just take the window."
  (interactive)
  (let ((split-point (point)))
    (save-excursion
      (save-restriction
        (split-window-below (1+ (mw-visual-line-number)))
        (goto-char split-point)
        (recenter -1)
        ))
    (other-window 1)
    (forward-line 1)
    (recenter 0)
    (other-window -1)))
;; #+end_src

;; ** Horizontally Split window below point

;; *** Code

;; #+begin_src emacs-lisp
(defun mw-visual-column-number ()
  "Visual column number of point."
  (save-excursion
    (let ((end-point (point)))
      (beginning-of-line)
      (1+ (- end-point (point))))))

(defun mw-split-window-horizontally-at-point ()
  "Split window horizontally at point."
  (interactive)
  (let ((split-point (point)))
    (save-excursion
      (save-restriction
        (split-window-right (mw-visual-column-number))))))

;; #+end_src

;; ** Kill outline-path

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-org-outline-path-starting-with-file ()
  "Return outline-path starting with file."
  (org-display-outline-path t (not (org-before-first-heading-p)) nil t))

(defun mw-org-outline-path-kill ()
  "Add the outline path to where point is to the `kill-ring'."
  (interactive)
  (let ((outline-path (mw-org-outline-path-starting-with-file)))
    (kill-new outline-path)
    (message "'%s' added to kill-ring" outline-path)))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:32] Using this sometimes before using org-refile
;; when I know to where the refile shall go.  At the refile I have just
;; to yank the outline-path.

;; ** Append to scratch

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-append-to-scratch (beg end)
  "Append region to buffer scratch."
  (interactive "r")
  (append-to-buffer (get-buffer-create "*scratch*") beg end))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:33] Using this occasionally.

;; ** Buddy buffer

;; *** Code
;; :PROPERTIES:
;; :ID:       f5a21b20-b311-40c4-9569-7b1175880ec9
;; :END:

;; #+BEGIN_SRC emacs-lisp
(defvar *mw-buddy-buffer* (current-buffer))

(defun mw-set-current-as-buddy ()
  "Set current buffer as buddy buffer."
  (interactive)
  (setq *mw-buddy-buffer* (current-buffer))
  (message "'%s' is buddy buffer now." (buffer-name *mw-buddy-buffer*)))

(defun mw-exchange-to-buddy ()
  "Make buddy buffer current buffer and vice versa."
  (interactive)
  (let ((other (current-buffer)))
    (and (buffer-live-p *mw-buddy-buffer*)
         (not (eq other *mw-buddy-buffer*))
         (switch-to-buffer *mw-buddy-buffer*))
    (if (eq other *mw-buddy-buffer*)
        (message "Already on buddy buffer.  '%s' remains buddy buffer."
                 (buffer-name *mw-buddy-buffer*))
      (setq *mw-buddy-buffer* other)
      (message "'%s' is buddy buffer now." (buffer-name *mw-buddy-buffer*)))))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:33] Using this occasionally.  Actually I thought
;; this would be a killer feature but it is not for me.

;; ** Umlautify

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-umlautify-before-point ()
  "Change character before point to its umlaut."
  (interactive)
  (let* ((conversion-table
         '((?a . "ä")
           (?o . "ö")
           (?u . "ü")
           (?s . "ß")
           (?A . "Ä")
           (?O . "Ö")
           (?U . "Ü")))
        (conversion (assoc (char-before) conversion-table)))
    (when conversion
          (delete-char -1)
          (insert (cdr conversion)))))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:34] Good.  Using it often.

;; ** Kill Buffer and Buffer Filename

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-kill-buffer-name ()
  "Append buffer-name to `kill-ring'."
  (interactive)
  (when (buffer-name)
    (kill-new (buffer-name))
    (message "%s entered kill-ring" (buffer-name))))

(defun mw-kill-buffer-filename ()
  "Append buffer-filename to `kill-ring'."
  (interactive)
  (let ((bfn (buffer-file-name)))
  (when bfn
    (kill-new bfn)
    (message "%s entered kill-ring" bfn))))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:35] Using this almost never.

;; ** Display all appointments

;; *** About

;; Display all appointments of the Emacs calendar.  This function
;; completes the appt functions AFAICT.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun lh-mw-appt-show ()
"List all appointments."
  (interactive)
  (let ((appt-list appt-time-msg-list))
    (with-current-buffer (get-buffer-create "*appts")
      (setq buffer-read-only nil)
      (erase-buffer)
      (while appt-list
        (insert (format "%s\n"
                 (substring-no-properties (cadar appt-list) 0)))
        (setq appt-list (cdr appt-list)))))
  (switch-to-buffer "*appts")
  (view-mode)
  (message "Get rid of this buffer pressing 'C'.  (Hint: view-mode)"))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:35] Using this occasionally.

;; ** sha1 of region

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-sha1-of-region-to-kill-ring (start end)
  "See sha1 docu for the parameters."
  (interactive "r")
  (kill-new (sha1 (current-buffer) start end)))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:36] Not using this often but I _think_ this has
;; potential.

;; ** sha1 as defun name

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-sha1-of-defun ()
  "Put the sha1 of a function text into the kill ring.
The function text starts at the argument list and ends at the
last paren (exclusive)."
  (interactive)
  (save-excursion
    (let* ((start (progn
                    (beginning-of-defun)
                    (search-forward-regexp "\(" nil nil 2)
                    (backward-char)
                    (point)))
           (end (progn
                  (end-of-defun)
                  (backward-char)
                  (point))))
      (kill-new (sha1 (current-buffer) start end)))))

(defun mw-sha1-as-defun-name ()
  "Name a function with the sha1 hash of the function text.
The function text starts at the argument list and ends at the
last paren (exclusive)."
  (interactive)
  (save-match-data
    (save-excursion
     (let* ((start (progn
                     (beginning-of-defun)
                     (search-forward-regexp "\(" nil nil 2)
                     (backward-char)
                     (point)))
            (end (progn
                   (end-of-defun)
                   (backward-char)
                   (point)))
            (sha1 (sha1 (current-buffer) start end)))
       ;; relying on (point) is within the defun
       (progn
         (goto-char start)
         (skip-chars-backward " \t")
         (search-backward-regexp "[[:space:]]")
         (just-one-space))
       (delete-region (point) start)
       (insert sha1 " ")))))

(defalias 'mw-hash-as-defun-name 'mw-sha1-as-defun-name ) ; better find that fun.

;; #+END_SRC

;; ** Buffer copy with sha1 name

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw--create-sha1-hash-named-copy-of-buffer ()
  (let ((hash (sha1 (current-buffer))))
    (unless (get-buffer hash)
      (append-to-buffer hash (point-min) (point-max)))
    (get-buffer hash)))

(defun mw--create-sha1-hash-named-copy-of-region (start end)
  (cl-assert (<= start end))
  (save-restriction
    (narrow-to-region start end)
    (let* ((hash (sha1 (current-buffer))))
      (unless (get-buffer hash)
        (append-to-buffer hash start end))
      (get-buffer hash))))

(defun mw-create-sha1-hash-named-copy-of-buffer ()
  "Copy buffer and name with its hash and switch to it."
  (interactive)
  (switch-to-buffer (mw--create-sha1-hash-named-copy-of-buffer))
  (view-mode))

(defun mw-create-sha1-hash-named-copy-of-region (start end)
  "Copy region to buffer named with hash of its content."
  (interactive "r")
  (switch-to-buffer (mw--create-sha1-hash-named-copy-of-region start end))
  (setq buffer-read-only t))
;; #+END_SRC

;; ** File copy with sha1 name

;; *** Code

;; #+BEGIN_SRC emacs-lisp


(defun mw-copy-file-with-hash-name (filename)
  "BROKEN! Copy file FILENAME using essentially its hash as filename of
the copy."
  (interactive
   (find-file-read-args "Find file: " t)
   )
  ;; (when filename
  ;;   (find-file-literally filename)
  ;;   ;; (let ((target (concat
  ;;   ;;                (file-name-directory filename)
  ;;   ;;                (sha1 (current-buffer)) "-"
  ;;   ;;                (file-name-nondirectory buffer-file-name))))
  ;;   ;;   ;; (copy-file filename target)
  ;;   ;;   )
  ;;   )
  ;; (kill-buffer)
  )
;; #+END_SRC

;; ** Random Name

;; *** About

;; Save time to find a name if all you want is a name.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-random-name ()
  "Insert a random name at point."
  (interactive)
  (insert "name_" (format "%019d" (abs (random)))))

(defun name-0040310557124765049-test ()
  (let ((a 23))
    (while (< 0 a)
      (mw-random-name)
      (insert "\n")
      (setq a (- a 1)))))
;; #+END_SRC

;; *** Random

;; [2016-04-28 Thu 22:47] I see potential.

;; ** Extra Point

;; *** About

;; The feature Extra Point is just one extra location you can set or go
;; to.

;; A typical scenario for Extra Point is given when you are at a location
;; and you feel you want to go somewhere else but you are quite sure you
;; want to come back to the current location.

;; *** Interface

;; Set the Extra Point with

;; #+begin_example
;; M-x mw-set-extra-point
;; #+end_example

;; Go to the Extra Point with

;; #+begin_example
;; M-x mw-goto-extra-point
;; #+end_example

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defvar *mw-extra-point* nil)

(defun mw-set-extra-point ()
  "Save current position as Extra Position."
  (interactive)
  (setf *mw-ariadne-point* (cons (current-buffer) (point)))
  (message "Extra point has been set."))

(defun mw-goto-extra-point ()
  "Set current cursor to the Extra Point if possible."
  (interactive)
  (when *mw-ariadne-point*
    (if (buffer-live-p (car *mw-ariadne-point*))
        (progn
          (switch-to-buffer (car *mw-ariadne-point*))
          (goto-char (cdr *mw-ariadne-point*))
          (message "At current Extra Point."))
      (message "Warning: Extra Point not found."))))
;; #+END_SRC

;; *** Use case

;; One user set these commands to key-chords and uses them occasionally.

;; *** Rating

;; [2016-04-28 Thu 22:47] Using occasionally.

;; 
;; ** Switch external minibuffer                                           :lab:

;; *** About

;; This is for setting the minibuffer into an extra frame.

;; Get back a minibuffer in the frame using ~M-x
;; mw-set-minibuffer-in-default-frame~ ~C-x 5 2~ ~C-x 5 1~.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-unset-minibuffer-in-default-frame ()
  (interactive)
  (setf (cdr (assoc 'minibuffer default-frame-alist)) nil))

(defun mw-set-minibuffer-in-default-frame ()
  (interactive)
  (if (assoc 'minibuffer default-frame-alist)
      (setf (cdr (assoc 'minibuffer default-frame-alist)) t)
    (setf default-frame-alist
          (append (list (cons 'minibuffer t )) default-frame-alist))))
;; #+END_SRC

;; *** Rating

;; [2016-04-28 Thu 22:36] Not using this often but I think this has
;; potential.  Possibly this is also too buggy still.

;; [2016-04-28 Thu 22:43] This looks broken.

;; [2016-11-05 Sat 15:27] This looks good.  I like this extra minibuffer
;; frame setting somehow.

;; ** Hours in the current day

;; *** About

;; Time of day as float.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-current-time-of-day-decimal ()
  "Return the decimal hours of the time of day.
E.g. 11.9 is almost noon, 12.0 is noon.
"
  (let ((time (current-time)))
    (+ (string-to-number (format-time-string "%H" time))
       (/ (string-to-number (format-time-string "%M" time)) 60.0))))
;; #+END_SRC

;; *** Rating

;;  - [2016-04-28 Thu 22:35] Forgotten about this function.
;;  - [2016-11-05 Sat 15:30] Again ;)

;; ** Exterminate top of kill ring

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-kill-ring-exterminate-top ()
  "Exterminate the current element from the `kill-ring'."
  (interactive)
  (setq kill-ring (cdr kill-ring))
  (setq kill-ring-yank-pointer kill-ring))
;; #+END_SRC

;; *** Rating

;; - [2016-04-28 Thu 22:34] Not using this.
;; - [2016-10-11 Tue 11:15] Just used this function.
;; - [2016-11-05 Sat 15:32] So at least one use!

;; ** Freeze Emacs Time                                                    :lab:

;; *** About

;; **** Analogy

;; Like in those SF films when the time stands still and the hero can
;; manipulate everything.

;; **** Notes

;;
;; Note that this freeze here pertains not every time-source of Emacs.
;; AFAIHS ~(format-time-string "%X")~ gets its data somewhere else.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(let (freeze-time-day)
  (defun freeze-time-adviser (x)
    (append (date-to-time (concat freeze-time-day " 11:55")) (list 0 0)))

  (defun freeze-time-unfreeze ()
    (interactive)
    (if (advice-member-p #'freeze-time-adviser #'current-time)
        (advice-remove #'current-time #'freeze-time-adviser)))

  (defun freeze-time-to (yyyy-mm-dd-date-string)
    "Advice `current-time' to return time YYYY-MM-DD-DATE-STRING at 11:55am."
    (interactive (list (org-read-date)))
    (freeze-time-unfreeze)
    (setf freeze-time-day yyyy-mm-dd-date-string)
    (advice-add #'current-time :filter-return #'freeze-time-adviser)))
;; #+END_SRC

;; *** Rating

;;  - [2016-11-05 Sat 15:32] Using this occasionally to change history.

;; ** Refile to known location                                    :lab:

;; *** About

;; Use bookmark `org-refile-direct-target' as target for the refile.

;; This functionality is for saving the time to wait for the
;; completion of the possible candidates for the case when you already
;; know where the refile should go.

;; Of course you must have set the target first.  This means setting
;; bookmark `org-refile-direct-target'.  This can be done with
;; function `mw-org-refile-set-direct-target-bm' or by foot via
;; setting the apropriate bookmark.  E.g. at the target point set the
;; mark with C-x rm `org-refile-direct-target'.

;; =M-x `mw-org-refile-refile-to-direct-target'= refiles the current
;; outline to the `org-refile-direct-target' bookmark.

;; Reminder: Get the list of bookmarks with C-x rl.
;; `counsel-bookmark' is an alternative.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-org-refile-set-direct-target-bm ()
  "Set to bookmark `org-refile-direct-target' to current point."
  (interactive)
  (bookmark-set "org-refile-direct-target")
  (message "bookmark org-refile-direct-target set"))
;; #+END_SRC

;; #+BEGIN_SRC emacs-lisp
(defun mw-org-refile-refile-to-direct-target ()
  "Refile to bookmark `org-refile-direct-target'."
  (interactive)
  (let ((file (bookmark-get-filename "org-refile-direct-target"))
        (pos (bookmark-get-position "org-refile-direct-target")))
    (if (not file)
        (message "Doing nothing.  Bookmark `org-refile-direct-target' not set")
      (org-refile nil (current-buffer) `(nil ,file nil ,pos)))))
;; #+END_SRC

;; *** Rating

;;  - [2016-11-05 Sat 15:35] Used this occasionally.

;; ** Refile DWIM           :lab:

;; Does not work for refiling to the same buffer.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun org-other-org-win ()
  "Return next best visible window in org mode."
  (get-window-with-predicate
   (lambda (window)
     (with-current-buffer (window-buffer window)
       (eq major-mode 'org-mode)))))

(defun org-refile-dwim ()
  "Refile to position in other Org window.
Inspired by dired operations with `dired-dwim-target' t."
  (interactive)
  (if-let ((win (org-other-org-win)))
      (unless (eq (selected-window) win)
        (save-window-excursion
          (select-window win)
          (mw-org-refile-set-direct-target-bm))
        (mw-org-refile-refile-to-direct-target))))
;; #+END_SRC

;; ** Ack for projectile

;; *** About

;; Ack for projectile.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun projectile-ack (search-term)
  "Run an ack search with SEARCH-TERM in the project."
  (interactive
   (list (read-from-minibuffer
          (projectile-prepend-project-name (format "Ack search for: "))
          (projectile-symbol-or-selection-at-point))))
  (if (require 'ack nil 'noerror)
      (ack (concat "ack " search-term) (projectile-project-root))
    (error "Package 'ack' is not available")))
;; #+END_SRC

;; *** Rating

;; - [2016-11-05 Sat 15:36] This is not necessary since there are very
;;   similar tools around.  E.g. 'ag'.

;; ** Wget

;; *** About

;; Get a file from the net.

;; - Scenaria:
;;  - You have an address of a file in the web.
;;  - You want a copy of that file in the current directory.

;; *** Code

;; **** wget with external wget

;; #+BEGIN_SRC emacs-lisp
(defun wget (url)
  "Download URL."
  (interactive (list (read-string "URL: ")))
  (call-process "wget" nil nil nil url)
  ;; (url-retrieve url 'wget-render
  ;;       	(list url (point) (current-buffer)))
  )
;; #+END_SRC

;; **** wget made in Emacs

;; Wget realized with Emacs' intrinsic powers.  This might be a weak
;; realization of wget.

;; #+BEGIN_SRC emacs-lisp
(defun raw-browse (url)
  "Fetch URL to a buffer."
  (interactive
   (list (read-string "URL: " nil nil nil)))
  (url-retrieve
   url
   (lambda (status url &optional point buffer encode)
     (message "url: %s, status: %s" url status)
     (let ((buffer-name (concat  "*raw " url "*")))
       (append-to-buffer
        buffer-name
        (point-min) (point-max))
       (switch-to-buffer buffer-name)))
   (list url nil (current-buffer))))
;; #+END_SRC

;; *** Rating

;; ** Repeat last command

;; *** About

;; Repeat the last command of the extended command history.

;; The functionality is (of course) already long available in Emacs.  Do
;; e.g. M-x M-p RET and there you are.

;; So this section is about to shorten this already short dance.

;; This is very experimental.  E.g. breaks when choosing a command that
;; needs a parameter.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-repeat-last-command ()
  "Execute first command in `extended-command-history'."
  (interactive)
  (funcall-interactively
   (read
    (car
     (remove-if
      (lambda (x) (string= "mw-repeat-last-command" x))
      extended-command-history)))))
;; #+END_SRC

;; #+BEGIN_SRC emacs-lisp
(defun mw-message-last-command ()
  "Display the command which would be activated by `mw-repeat-last-command'."
  (interactive)
  (message
   (car (remove-if
         (lambda (x) (string= "mw-repeat-last-command" x))
         extended-command-history))))
;; #+END_SRC

;; **** TODO fix code to only do the necessary

;; *** Rating

;;  - [2016-11-05 Sat 15:44] Using this often.  Bound to C-6.
;;  - [2016-11-05 Sat 15:45] The command is fragile.  E.g. fails for
;;    commands which expect parameters.

;; ** Convert to double-space sentence

;; *** About

;; Sometimes text is formatted to end with one space at the ends of
;; sentences but the user want it to be two space.

;; *** Code

;; #+BEGIN_SRC emacs-lisp
(defun mw-convert-next-sentence-end-double-space ()
  "Go forward a sentence and add a SPC when there is only one yet."
  (interactive)
  (let ((sentence-end-double-space nil))
    (forward-sentence)
    (when (looking-at " [^ ]") (insert " "))))
;; #+END_SRC

;; *** Links
;; *** Rating

;; ** Template for a new helper                                       :noexport:

;; *** About
;; *** Code
;; #+BEGIN_SRC emacs-lisp
;; #+END_SRC
;; *** Links
;; *** Rating

;;; Tail:                                                             :noexport:

;; ** Provide Emacs lib

;; #+BEGIN_SRC emacs-lisp
(provide 'little-helpers)
;; #+END_SRC

;; ** Closing notes

;; 
;; # Local _DISABLED_ Variables:
;; # lentic-init: lentic-orgel-org-init
;; # End:

;;; little-helpers.el ends here
