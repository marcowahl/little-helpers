;;; org-supplements-tests.el --- some tests -*- lexical-binding: t ; -*-

;; (eval-buffer) for testing.  no message means tests pass.

(require 'org-supplements)

(progn

  (with-temp-buffer
    (org-mode)
    (insert "* a
** b
*** c
CCC
*** d
** foo
** bar
* a
** b")
    (goto-char (point-min))
    (assert (= 3 (org--count-sub-orgees))))

  (with-temp-buffer
    (org-mode)
    (insert "* a
** b
* a
** b")
    (goto-char (point-min))
    (assert (= 1 (org--count-sub-orgees))))

  (with-temp-buffer
    (org-mode)
    (insert "* a
** b")
    (goto-char (point-min))
    (assert (= 1 (org--count-sub-orgees))))

  (with-temp-buffer
    (org-mode)
    (insert "* a")
    (goto-char (point-min))
    (assert (= 0 (org--count-sub-orgees)))))


;;; org-supplements-tests.el ends here
