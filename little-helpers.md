<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgb2c7ac8">1. Alternate Up</a>
<ul>
<li><a href="#org4b84dbd">1.1. About</a></li>
<li><a href="#org017f223">1.2. Interface</a></li>
<li><a href="#org4ce96cc">1.3. Suggestions</a></li>
<li><a href="#orgb752f2d">1.4. Code</a></li>
<li><a href="#org33e0249">1.5. Links</a></li>
<li><a href="#org3049dfc">1.6. Rating</a></li>
</ul>
</li>
<li><a href="#org90601b7">2. Mode line as message</a>
<ul>
<li><a href="#org1b1d2a7">2.1. About</a></li>
<li><a href="#org8ee993e">2.2. Interface</a></li>
<li><a href="#orgd889c43">2.3. Code&#xa0;&#xa0;&#xa0;<span class="tag"><span class="ATTACH">ATTACH</span></span></a></li>
<li><a href="#orgba9e7be">2.4. Rating</a></li>
</ul>
</li>
<li><a href="#org0b2f07b">3. Line duplication</a>
<ul>
<li><a href="#orgfe6a45d">3.1. About</a></li>
<li><a href="#org08211fa">3.2. Interface</a></li>
<li><a href="#org4ac8f01">3.3. Code</a></li>
<li><a href="#orgceb8932">3.4. Links</a></li>
<li><a href="#org088a01d">3.5. Rating</a></li>
</ul>
</li>
<li><a href="#org557c06b">4. Operate on Numbers in String</a>
<ul>
<li><a href="#org439728f">4.1. About</a></li>
<li><a href="#org13a2a16">4.2. Interface</a></li>
<li><a href="#org149fb8f">4.3. Code</a></li>
<li><a href="#orge5f5074">4.4. Links</a></li>
<li><a href="#org9f0cffb">4.5. Rating</a></li>
</ul>
</li>
<li><a href="#orgab3f74a">5. visible-mode control for ediff</a>
<ul>
<li><a href="#org0479199">5.1. About</a></li>
<li><a href="#orga8232f0">5.2. Interface</a></li>
<li><a href="#org9959a4b">5.3. Code</a></li>
<li><a href="#org57482b9">5.4. Rating</a></li>
<li><a href="#org7b51120">5.5. Rest</a></li>
</ul>
</li>
<li><a href="#org88b5c56">6. Switch Comma and Dot</a>
<ul>
<li><a href="#orgd05a503">6.1. About</a></li>
<li><a href="#org898053a">6.2. Interface</a></li>
<li><a href="#org4292307">6.3. Code</a></li>
<li><a href="#org3b56ac6">6.4. Links</a></li>
<li><a href="#org0f5615b">6.5. Rating</a></li>
</ul>
</li>
<li><a href="#orge698ad4">7. Jump to line and recenter to top</a>
<ul>
<li><a href="#org49c3f5d">7.1. About</a></li>
<li><a href="#org7fa13b5">7.2. Interface</a></li>
<li><a href="#orge5b22ce">7.3. Dependencies</a></li>
<li><a href="#org3b01aa5">7.4. Code</a></li>
<li><a href="#org52ad0c0">7.5. Links</a></li>
<li><a href="#orga5f5e3a">7.6. Rating</a></li>
</ul>
</li>
<li><a href="#org4cb3692">8. Delete blank lines also above&#xa0;&#xa0;&#xa0;<span class="tag"><span class="lab">lab</span></span></a>
<ul>
<li><a href="#org3b766c7">8.1. About</a></li>
<li><a href="#org2926606">8.2. Interface</a></li>
<li><a href="#org02ceec2">8.3. Code</a></li>
<li><a href="#org5cfbaee">8.4. Rating</a></li>
</ul>
</li>
<li><a href="#org405a67a">9. Save Buffer Encrypted for Certain Key&#xa0;&#xa0;&#xa0;<span class="tag"><span class="lab">lab</span></span></a>
<ul>
<li><a href="#orgbc4834c">9.1. About</a></li>
<li><a href="#org7dac0aa">9.2. Interface</a></li>
<li><a href="#orgb464b2b">9.3. Code</a></li>
<li><a href="#org3211ffe">9.4. Rating</a></li>
</ul>
</li>
<li><a href="#orga3026cd">10. Mark first key in epa key list buffer&#xa0;&#xa0;&#xa0;<span class="tag"><span class="lab">lab</span></span></a>
<ul>
<li><a href="#org2f466b8">10.1. About</a></li>
<li><a href="#org6fd62c2">10.2. Interface</a></li>
<li><a href="#org290f945">10.3. Code</a></li>
<li><a href="#orgc9703f3">10.4. Links</a></li>
<li><a href="#orgca643b2">10.5. Rating</a></li>
</ul>
</li>
<li><a href="#orge598007">11. Ephemeral decoration</a>
<ul>
<li><a href="#org91b861c">11.1. About</a></li>
<li><a href="#orgdf40b03">11.2. Interface</a></li>
<li><a href="#org1f46dc6">11.3. Code</a></li>
<li><a href="#org71a60f2">11.4. Rest</a></li>
</ul>
</li>
<li><a href="#orgafacd91">12. Print numbers for visual lines&#xa0;&#xa0;&#xa0;<span class="tag"><span class="lab">lab</span></span></a>
<ul>
<li><a href="#orgf177015">12.1. About</a></li>
<li><a href="#org31c5a09">12.2. Interface</a></li>
<li><a href="#org7203ca0">12.3. Code</a></li>
<li><a href="#orge485148">12.4. Links</a></li>
<li><a href="#orgb1132fd">12.5. Rating</a></li>
</ul>
</li>
<li><a href="#orgbc31de1">13. Split window below point</a>
<ul>
<li><a href="#org04a6549">13.1. Code</a></li>
</ul>
</li>
<li><a href="#orgac345df">14. Horizontally Split window below point</a>
<ul>
<li><a href="#orgaa553db">14.1. Code</a></li>
</ul>
</li>
<li><a href="#org8c6bd9f">15. Kill outline-path</a>
<ul>
<li><a href="#org8a51fd5">15.1. Code</a></li>
<li><a href="#orgbf77ac5">15.2. Rating</a></li>
</ul>
</li>
<li><a href="#org7362685">16. Append to scratch</a>
<ul>
<li><a href="#org018fb75">16.1. Code</a></li>
<li><a href="#orgcc65dc1">16.2. Rating</a></li>
</ul>
</li>
<li><a href="#orgc1942bd">17. Buddy buffer</a>
<ul>
<li><a href="#orge73bd13">17.1. Code</a></li>
<li><a href="#org9e11cc2">17.2. Rating</a></li>
</ul>
</li>
<li><a href="#org9fa452a">18. Umlautify</a>
<ul>
<li><a href="#orgdda680b">18.1. Code</a></li>
<li><a href="#org6658666">18.2. Rating</a></li>
</ul>
</li>
<li><a href="#org69a9d56">19. Kill Buffer Filename</a>
<ul>
<li><a href="#org03f3ddc">19.1. Code</a></li>
<li><a href="#org264ccc7">19.2. Rating</a></li>
</ul>
</li>
<li><a href="#org80a2329">20. Display all appointments</a>
<ul>
<li><a href="#orgb45bcd5">20.1. About</a></li>
<li><a href="#org8b7ce59">20.2. Code</a></li>
<li><a href="#org798d699">20.3. Rating</a></li>
</ul>
</li>
<li><a href="#orgff996b0">21. sha1 of region</a>
<ul>
<li><a href="#org5e00c22">21.1. Code</a></li>
<li><a href="#org3f148ce">21.2. Rating</a></li>
</ul>
</li>
<li><a href="#org305afcb">22. Insert Random Name</a>
<ul>
<li><a href="#org6c01760">22.1. About</a></li>
<li><a href="#orgdeef79d">22.2. Code</a></li>
<li><a href="#orgbeee554">22.3. Random</a></li>
</ul>
</li>
<li><a href="#orgcc04b04">23. Extra Point</a>
<ul>
<li><a href="#orge28f91c">23.1. About</a></li>
<li><a href="#org29877ce">23.2. Interface</a></li>
<li><a href="#orgb30247e">23.3. Code</a></li>
<li><a href="#orgcad2c08">23.4. Use case</a></li>
<li><a href="#orgc30e792">23.5. Rating</a></li>
</ul>
</li>
<li><a href="#orga9309bf">24. Switch external minibuffer&#xa0;&#xa0;&#xa0;<span class="tag"><span class="lab">lab</span></span></a>
<ul>
<li><a href="#org5c52d27">24.1. About</a></li>
<li><a href="#orgf8ea6b5">24.2. Code</a></li>
<li><a href="#org51afb84">24.3. Rating</a></li>
</ul>
</li>
<li><a href="#org9f3b403">25. Hours in the current day</a>
<ul>
<li><a href="#orgbc837aa">25.1. About</a></li>
<li><a href="#orgc43857d">25.2. Code</a></li>
<li><a href="#orga65e04c">25.3. Rating</a></li>
</ul>
</li>
<li><a href="#org3546eed">26. Exterminate top of kill ring</a>
<ul>
<li><a href="#org6410235">26.1. Code</a></li>
<li><a href="#org3de9cb0">26.2. Rating</a></li>
</ul>
</li>
<li><a href="#org23c0c7f">27. Freeze Emacs Time&#xa0;&#xa0;&#xa0;<span class="tag"><span class="lab">lab</span></span></a>
<ul>
<li><a href="#org625e068">27.1. About</a></li>
<li><a href="#org3374441">27.2. Code</a></li>
<li><a href="#orgdc51e06">27.3. Rating</a></li>
</ul>
</li>
<li><a href="#org62498ff">28. Refile to known location&#xa0;&#xa0;&#xa0;<span class="tag"><span class="lab">lab</span></span></a>
<ul>
<li><a href="#orgba1f125">28.1. About</a></li>
<li><a href="#org872a3b3">28.2. Code</a></li>
<li><a href="#org271e57c">28.3. Rating</a></li>
</ul>
</li>
<li><a href="#org1863da8">29. Ack for projectile</a>
<ul>
<li><a href="#org4667aef">29.1. About</a></li>
<li><a href="#orgb00bda1">29.2. Code</a></li>
<li><a href="#org584146c">29.3. Rating</a></li>
</ul>
</li>
<li><a href="#org2f0a761">30. Wget</a>
<ul>
<li><a href="#orgcaf680f">30.1. About</a></li>
<li><a href="#orgc39d44f">30.2. Code</a></li>
<li><a href="#org96a3744">30.3. Rating</a></li>
</ul>
</li>
<li><a href="#org25ed1e4">31. Repeat last command</a>
<ul>
<li><a href="#orgf2c7bf5">31.1. About</a></li>
<li><a href="#org6d2eeef">31.2. Code</a></li>
<li><a href="#orgd49b753">31.3. Rating</a></li>
</ul>
</li>
</ul>
</div>
</div>

<a id="orgb2c7ac8"></a>

# Alternate Up


<a id="org4b84dbd"></a>

## About

Command `mw-alternate-up` replaces a buffer with a buffer which is up.
This is like zooming out.

The most relevant examples are:

-   A dired gets replaced by its parent dired.
-   A file gets replaced by its dired.

The function helps to keep the buffer-list small.  It is a pendant
(and kind of inverse) to function `dired-find-alternate-file` which
typically is bound to "a".  This is where the 'alternate' comes from.

The binding to "\`" for dired-mode and key-chord "\` \`" for the general
case have been reported to work well for at least one user.


<a id="org017f223"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`mw-alternate-up`</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org4ce96cc"></a>

## Suggestions

Extend dired with key "\`" for zoom out.

    (add-hook
     'dired-mode-hook
     (lambda ()
       (define-key dired-mode-map "`"
         #'mw-alternate-up)))

Bind key-chord "\` \`" to zoom out.  Of course the key-chord lib must be
available.  See [Links](#org33e0249).

    (key-chord-define-global "``" #'mw-alternate-up)


<a id="orgb752f2d"></a>

## Code

    (defun mw-alternate-up ()
      "Replace buffer with the corresponding dired one level up.

      Further try to move point to the file or directory which was
      the origin for the up-action.  Kill if dired already root folder.

      This function is very similar to ‘dired-up-directory’.  Note
      that ‘mw-alternate-up’ tries to delete the
      buffer from which the function has been called.  This function
      is supposed to help keeping the ‘buffer-list’ small."
      (interactive)
      (let ((dir
             (or buffer-file-name
                 (cond ((eq major-mode 'dired-mode) (dired-current-directory))
                       ((eq major-mode 'magit-status-mode) (magit-git-dir))
                       ((eq major-mode 'eshell-mode)
                        (concat (eshell/pwd) "/trick!haha"))
                       ((eq major-mode 'compilation-mode)
                        (concat compilation-directory "/trick!haha"))
                       ((and (eq major-mode 'eww-mode)
                             (string-match-p
                              "file://" (eww-current-url)))
                        (substring (eww-current-url) (length "file://")))
                       (t (message (concat
                                    "Doing nothing."
                                    "  No alternative for going up is set for this kind of buffer."))
                          nil)))))
        (when dir
          (cond ((and (eq major-mode 'dired-mode) (string-equal "/" dir)) (kill-buffer))
                (t (progn
                     (find-alternate-file
                      (file-name-directory (directory-file-name dir)))
                     (dired-goto-file dir)))))))


<a id="org33e0249"></a>

## Links

<http://melpa.org/#/key-chord>


<a id="org3049dfc"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 19:52] </span></span> Rocks!
<span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:15] </span></span> Still rocks!


<a id="org90601b7"></a>

# Mode line as message


<a id="org1b1d2a7"></a>

## About

Function `mw-display-mode-line-as-message` displays the modeline as a
message.  This can help when the modeline is too small for its
contents.  As a message the whole information of the modeline
typically gets displayed.


<a id="orge954997"></a>

### Illustration

![img](./data/b8/804ee5-e436-48af-ba09-f329c1d00f17/emacsshot.fra-20150615220608.png)


<a id="org8ee993e"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`mw-display-mode-line-as-message`</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="orgd889c43"></a>

## Code     :ATTACH:

It's essential to quote the \`%' characters.

    (defun mw-quote-%-with-% (string)
      "Replace all occurances of '%' with '%%'.
    Argument STRING input text."
      (let ((pos 0))
        (while (string-match "%" string pos)
          (setq string (replace-match "%%" nil nil string))
          (setq pos (1+ (match-end 0))))
        string))

    (defun mw-mode-line-as-message ()
      "Display the modeline as message."
      (interactive)
      (message (mw-quote-%-with-% (format-mode-line mode-line-format))))


<a id="orgba9e7be"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 19:54] </span></span> Useful.


<a id="org0b2f07b"></a>

# Line duplication


<a id="orgfe6a45d"></a>

## About

Just duplicate the line containing the cursor.

To bind key chord 'yy' to `mw-duplicate-line` might be a reliable
choice.

<span class="timestamp-wrapper"><span class="timestamp">[2015-10-06 Tue 23:48] </span></span> I use this function a lot.


<a id="org08211fa"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`mw-duplicate-line`</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org4ac8f01"></a>

## Code

    (defun mw-duplicate-line ()
      "Duplicate the line containing point."
      (interactive)
      (save-excursion
        (beginning-of-line)
        (insert "\n" (buffer-substring
                      (point) (progn
                                (end-of-line)
                                (point))))))


<a id="orgceb8932"></a>

## Links

I can easily imagine that there are lots of programs out there which
do the line duplication already.

Consider also avy-copy-line of Emacs package avy.  Which has a
different touch.

<http://melpa.org/#/key-chord>


<a id="org088a01d"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 19:54] </span></span> Useful.


<a id="org557c06b"></a>

# Operate on Numbers in String


<a id="org439728f"></a>

## About

Functions for operating on the last number in a string.


<a id="org13a2a16"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">name</th>
<th scope="col" class="org-left">arguments</th>
<th scope="col" class="org-left">what</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">`mw-increment-last-number`</td>
<td class="org-left">string</td>
<td class="org-left">defun</td>
</tr>


<tr>
<td class="org-left">`mw-decrement-last-number`</td>
<td class="org-left">string</td>
<td class="org-left">defun</td>
</tr>


<tr>
<td class="org-left">`mw-operate-on-last-number`</td>
<td class="org-left">string fun</td>
<td class="org-left">defun</td>
</tr>


<tr>
<td class="org-left">`mw-fill-with-zeros`</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">variable</td>
</tr>
</tbody>
</table>


<a id="org149fb8f"></a>

## Code

    (defvar mw-fill-with-zeros t
      "nil means to neither introduce nor respect given leading zeros
      in numbers for the string operations.  Non-nil means to keep
      the width for a given number by using the respective number of
      leading zeros.")

    (defun mw-increment-last-number (string)
      "Increase the last number in STRING by one."
      (mw-operate-on-last-number string '1+))

    (defun mw-decrement-last-number (string)
      "Decrease the last number in STRING by one."
      (mw-operate-on-last-number string '1-))

    (defun mw-operate-on-last-number (string fun)
      "Apply FUN on the last number in STRING."
      (when (string-match "\\([[:digit:]]+\\)[^[:digit:]]*$" string)
        (let* ((number-string (match-string 1 string))
    	   (operated-number-string
    	    (number-to-string (funcall fun (string-to-number number-string)))))
          (replace-match
           (concat
            (if mw-fill-with-zeros
                (let ((length-diff
                       (- (length number-string)
                          (length operated-number-string))))
                  (if (< 0 length-diff)
                      (make-string length-diff ?0)
                    ""))
              "")
            operated-number-string)
           nil nil string 1))))


<a id="orge5f5074"></a>

## Links

<http://www.emacswiki.org/emacs/IncrementNumber> (discovered
<span class="timestamp-wrapper"><span class="timestamp">[2015-06-17 Wed 19:27]</span></span>)


<a id="org9f0cffb"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 19:55] </span></span> Not sure but I think this function is used somewhere.


<a id="orgab3f74a"></a>

# visible-mode control for ediff


<a id="org0479199"></a>

## About

Enable visible-mode for the buffers in a ediff session.  This can be
helpful e.g. for org-mode buffers which often have a lot of invisible
parts.


<a id="orga8232f0"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`mw-ediff-set-visible-mode-in-ediff-buffers`</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org9959a4b"></a>

## Code

    (defun mw-ediff-set-visible-mode-in-ediff-buffers ()
    "Set ‘visible-mode’ in all buffers of a ediff session.
    Only functional in an ediff control buffer."
      (interactive)
      (ediff-barf-if-not-control-buffer)
      (message "ediff buffers set to visible-mode to reveal hidden parts.
    M-x visible-mode toggles visible-mode for a buffer.")
      (when ediff-buffer-A (save-excursion (set-buffer ediff-buffer-A)
                                           (visible-mode)))
      (when ediff-buffer-B (save-excursion (set-buffer ediff-buffer-B)
                                           (visible-mode)))
      (when ediff-buffer-C (save-excursion (set-buffer ediff-buffer-C)
                                           (visible-mode))))


<a id="org57482b9"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 19:56] </span></span> Useful but using it very rare.


<a id="org7b51120"></a>

## Rest


<a id="org2609d23"></a>

### Reset visible-mode on quitting ediff


<a id="org88b5c56"></a>

# Switch Comma and Dot


<a id="orgd05a503"></a>

## About

Sometimes it helps to have a tool to change 5.000,00 to 5,000.00
quickly.  In Germany they often use 5.000,00 while in the US they use
5,000.00.

Concretely this has been useful for bringing some numbers into the
right format for the ledger program.


<a id="org898053a"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`mw-switch-comma-and-dot-in-region`</td>
<td class="org-left">command</td>
</tr>


<tr>
<td class="org-left">`mw-switch-comma-and-dot-in-string`</td>
<td class="org-left">defun</td>
</tr>
</tbody>
</table>


<a id="org4292307"></a>

## Code

Switch comma and dot in a string.

    (defun mw-switch-comma-and-dot-in-string (string-with-number)
      (with-temp-buffer
        (insert string-with-number)
        (goto-char (point-min))
        (while (search-forward-regexp "\[.,\]" nil t)
          (let ((insert-char (if (equal ?. (char-before)) ?, ?.)))
            (delete-char -1)
            (insert insert-char)))
        (buffer-string)))

Switch comma and dot in a region.

    (defun mw-switch-comma-and-dot-in-region (beg end)
      "Replace . with , and vice versa in region."
      (interactive "r")
      (let ((converted (mw-switch-comma-and-dot-in-string (buffer-substring beg end))))
        (delete-region beg end)
        (goto-char beg)
        (insert converted)))


<a id="org3b56ac6"></a>

## Links

<http://www.ledger-cli.org/index.html>


<a id="org0f5615b"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 19:56] </span></span> Useful but using it very rare.


<a id="orge698ad4"></a>

# Jump to line and recenter to top


<a id="org49c3f5d"></a>

## About

This is a realization to do the jump to a line and recenter that line
to the top of the window in one step.


<a id="org7fa13b5"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">mw-recenter-jump-to-top</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="orge5b22ce"></a>

## Dependencies

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">avy</td>
</tr>
</tbody>
</table>


<a id="org3b01aa5"></a>

## Code

    (defun mw-recenter-jump-to-top ()
      (interactive)
      (advice-add 'avy-goto-line :after (lambda () (recenter 0)))
      (avy-goto-line)
      (advice-remove 'avy-goto-line (lambda () (recenter 0))))


<a id="org52ad0c0"></a>

## Links

<https://github.com/winterTTr/ace-jump-mode/>
<https://github.com/abo-abo/avy>


<a id="orga5f5e3a"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:30] </span></span> Using this occasionally.


<a id="org4cb3692"></a>

# Delete blank lines also above     :lab:


<a id="org3b766c7"></a>

## About

Builtin function delete-blank-lines does delete following blank lines
when the cursor is in a non-blank line.  The extension here is to also
delete blank lines above.


<a id="org2926606"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`mw-delete-blank-lines`</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org926b4c8"></a>

### Binding

Replace the recent binding with

    (global-set-key  (kbd "C-x C-o") #'mw-delete-blank-lines)


<a id="org02ceec2"></a>

## Code

    (defun mw-delete-blank-lines ()
      "On blank line, delete all surrounding blank lines, leaving just one.
    On isolated blank line, delete that one.
    On nonblank line, delete any immediately following blank lines."
      (interactive "*")
      (let (thisblank singleblank)
        (save-excursion
          (beginning-of-line)
          (setq thisblank (looking-at "[ \t]*$"))
          ;; Set singleblank if there is just one blank line here.
          (and thisblank
               (not (looking-at "[ \t]*\n[ \t]*$"))
               (or (bobp)
                   (progn (forward-line -1)
                          (not (looking-at "[ \t]*$"))))))
        ;; Delete preceding blank lines, and this one too if it's the only one.
        (if thisblank
    	(progn
    	  (beginning-of-line)
    	  (if singleblank (forward-line 1))
    	  (delete-region (point)
    			 (if (re-search-backward "[^ \t\n]" nil t)
    			     (progn (forward-line 1) (point))
    			   (point-min)))))
        ;; Delete following blank lines, unless the current line is blank
        ;; and there are no following blank lines.
        (when (not (and thisblank singleblank))
          (save-excursion
            (end-of-line)
            (forward-line 1)
            (delete-region (point)
                           (if (re-search-forward "[^ \t\n]" nil t)
                               (progn (beginning-of-line) (point))
                             (point-max))))
          ;; lab-block...
          ;; mw 201507131114 delete also preceding blank lines
          (save-excursion
            (beginning-of-line)
            (delete-region (point)
                           (if (re-search-backward "[^ \t\n]" nil t)
                               (progn (forward-line 1) (point))
                             (point-min))))
          ;; mw 201507131114 delete also preceding blank lines
          ;; ...lab-block.
          )

        ;; Handle the special case where point is followed by newline and eob.
        ;; Delete the line, leaving point at eob.
        (if (looking-at "^[ \t]*\n\\'")
    	(delete-region (point) (point-max)))))


<a id="org5cfbaee"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 20:00] </span></span> I use this occasionally.  Looks a bit too complicated to me.


<a id="org405a67a"></a>

# Save Buffer Encrypted for Certain Key     :lab:


<a id="orgbc4834c"></a>

## About

When using the gpg suffix Emacs asks for a key or keys to encrypt for.
For my personal secrets I want the file encryted for me.  (Yes, I have
something to hide.)

This is an attempt to simplify the encryption process in this case.


<a id="org7dac0aa"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">`mw-save-buffer-encrypted-for-me`</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="orgb464b2b"></a>

## Code

    (defun mw-save-buffer-encrypted-for-me ()
      "Set certain 'encrypt to', ‘save-buffer’, restore 'encrypt to'.
    This function has an effect when saving gpg files and is thought
    as convenience for me.

    Hint: What about defadvice?  And further improvements?  Ideal: No
    disturbance when saving at all."
      (interactive)
      (let (recent-epa-file-encrypt-to epa-file-encrypt-to)
        (setq  epa-file-encrypt-to "49010A040A3AE6F2")
        (save-buffer)
        (setq  epa-file-encrypt-to recent-epa-file-encrypt-to)))


<a id="org3211ffe"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:31] </span></span> Never using this.


<a id="orga3026cd"></a>

# Mark first key in epa key list buffer     :lab:


<a id="org2f466b8"></a>

## About

Mark the next key in a epa key list buffer.  This is particularly
useful for marking the first key in the list.


<a id="org6fd62c2"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">mw-epa-mark-next-key</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org290f945"></a>

## Code

    (defun mw-epa-mark-next-key ()
      "Mark next ‘epa-key’.
    This defun is for working in a epa-key-listing buffer."
      (interactive)
      (let ((inhibit-read-only t)
    	buffer-read-only
    	properties)
        (goto-char (point-min))
        (goto-char (next-char-property-change (1+ (point))))
        (while (not (get-text-property (point) 'epa-key))
          (goto-char (next-char-property-change (1+ (point))))
          (if (eq (point) (point-max))
              (error "No key")))
        (setq properties (text-properties-at (point)))
        (delete-char 1)
        (insert "*")
        (set-text-properties (1- (point)) (point) properties)))


<a id="orgc9703f3"></a>

## Links

[Save Buffer Encrypted for Certain Key](#org405a67a)


<a id="orgca643b2"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:31] </span></span> Never using this.


<a id="orge598007"></a>

# Ephemeral decoration


<a id="org91b861c"></a>

## About

Decorate buffer with various information but just ephemerally.


<a id="orgdf40b03"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">mw-ephemeral-rectangle-enumerate-lines-sit</td>
<td class="org-left">command</td>
</tr>


<tr>
<td class="org-left">mw-ephemeral-rectangle-enumerate-lines-timer</td>
<td class="org-left">command</td>
</tr>
</tbody>
</table>


<a id="org1f46dc6"></a>

## Code

    (let (overlays)

      (defun mw-ephemeral-delete-overlays ()
        "Delete all overlays."
        (mapc #'delete-overlay overlays)
        (setq overlays nil))

      (defun mw-ephemeral-rectangle-enumerate-lines-sit (beg end)
        "Enumerate the region lines in the column of BEG.
    The enumeration disappears on any keypress or after a few
    seconds.  Argument END is the other end of the region."
        (interactive "r")
        (save-excursion
          (let ((col (- (progn (goto-char beg) (point))
                        (progn (beginning-of-line) (point))))
                (ctr 1)
                (fill-spaces-number 0))
            (goto-char beg)
            (while
                (progn
                  (push (make-overlay (point) (point))
                        overlays)
                  (overlay-put
                   (car overlays)
                   'after-string (format "%s %2d " (make-string fill-spaces-number 32) ctr))
                  (setq ctr (1+ ctr)
                        fill-spaces-number 0)
                  (forward-line)
                  (let ((linewidth (- (progn (end-of-line) (point))
                                      (progn (beginning-of-line) (point)))))
                    (if (< col linewidth)
                        (forward-char col)
                      (end-of-line)
                      (setq fill-spaces-number (- col linewidth))))
                  (< (point) end)))))
        (sit-for 9)
        (mw-ephemeral-delete-overlays))

      (defun mw-ephemeral-rectangle-enumerate-lines-timer (beg end)
        "Enumerate the region lines in the column of BEG.
    The enumeration disappears on any keypress or after a few
    seconds.  Argument END is the other end of the region."
        (interactive "r")
        (save-excursion
          (let ((col (- (progn (goto-char beg) (point))
                        (progn (beginning-of-line) (point))))
                (ctr 1)
                (fill-spaces-number 0))
            (goto-char beg)
            (while
                (progn
                  (push (make-overlay (point) (point))
                        overlays)
                  (overlay-put
                   (car overlays)
                   'after-string (format "%s %2d " (make-string fill-spaces-number 32) ctr))
                  (setq ctr (1+ ctr)
                        fill-spaces-number 0)
                  (forward-line)
                  (let ((linewidth (- (progn (end-of-line) (point))
                                      (progn (beginning-of-line) (point)))))
                    (if (< col linewidth)
                        (forward-char col)
                      (end-of-line)
                      (setq fill-spaces-number (- col linewidth))))
                  (< (point) end)))))
        (run-with-timer 9 nil #'mw-ephemeral-delete-overlays))

      (defun mw-ephemeral-enumerate-visual-lines ()
        "Enumerate the visual lines in the window.
    The numbering disappears when the user does soething in the
    window or after some time.

    Check the term visual line in case you are uncertain.

    This enumeration can help to find a prefix argument to use
    command `move-to-window-line'."
        (interactive)
        (let ((n 0)
              (lnmax-pos (save-excursion (move-to-window-line -2)))
              (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
          (save-excursion
            (while (and (<= (move-to-window-line n) lnmax-pos)
                        (< n line-number-enumerate-max))
              (push (make-overlay (point) (progn (end-of-visual-line) (point))) overlays)
              (overlay-put
               (car overlays) 'before-string
               (format "%3d " (1+ n)))
              (setq n (1+ n))))
          (sit-for 23)
          (mw-ephemeral-delete-overlays)))

      (defun mw-ephemeral-enumerate-visual-lines-down-from-point ()
        "Enumerate the visual lines down starting with the line containing point."
        (interactive)
        (let ((n 0)
              (line-number-with-point (mw-visual-line-number-with-point))
              (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
          (save-excursion
            (move-to-window-line (+ n line-number-with-point))
            (while (<= (+ line-number-with-point n) line-number-enumerate-max)
              (push (make-overlay (point) (point)) overlays)
              (overlay-put
               (car overlays) 'after-string
               (format "%3d " (1+ n)))
              (setq n (1+ n))
              (move-to-window-line (+ n line-number-with-point))))
          (sit-for 23)
          (mw-ephemeral-delete-overlays)))

      (defun mw-ephemeral-enumerate-visual-lines-relative-to-point ()
        "Enumerate the visual lines in the window relative to point."
        (interactive)
        (let ((n 0)
              (lnwp (mw-visual-line-number-with-point))
              (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
          (save-excursion
            (while (not (= line-number-enumerate-max (move-to-window-line n)))
              (push (make-overlay (point) (point)) overlays)
              (overlay-put
               (car overlays) 'after-string
               (format "%3d " (- n lnwp)))
              (setq n (1+ n)))
            (when (not (= (point-max) (progn (move-to-window-line -1) (point))))
              (push (make-overlay (point) (point)) overlays)
              (overlay-put
               (car overlays) 'after-string
               (format "%3d " (- n lnwp)))))
          (sit-for 23)
          (mw-ephemeral-delete-overlays)))

      (defun mw-ephemeral-enumerate-visual-lines-dist-to-border ()
        "Enumerate the visible lines in the window with distance to
    borders.

    This enumeration can help to find a prefix argument to use
    command `move-to-window-line'.
    "
        (interactive)
        (let ((n 0)
              (window-lines (mw-count-window-lines))
              (half-window-lines (/ (mw-count-window-lines) 2)))
          (while (<= n half-window-lines)
            (save-excursion
              (move-to-window-line n)
              (push (make-overlay (point) (point)) overlays)
              (overlay-put
               (car overlays) 'after-string
               (format "%3d " n)))
            (let ((m (- (1+ n))))
              (if (or (< n half-window-lines)
                      (oddp window-lines))
                  (save-excursion
                    (move-to-window-line m)
                    (push (make-overlay (point) (point)) overlays)
                    (overlay-put
                     (car overlays) 'after-string
                     (format "%3d " m)))))
            (setq n (1+ n)))
          (sit-for 23)
          (mw-ephemeral-delete-overlays))))


<a id="org71a60f2"></a>

## Rest


<a id="orgafacd91"></a>

# Print numbers for visual lines     :lab:


<a id="orgf177015"></a>

## About

Variations of counting the lines as they appear in an Emacs window.

This is different from counting the lines in a file or in a buffer.


<a id="org31c5a09"></a>

## Interface

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">mw-enumerate-visual-lines</td>
<td class="org-left">command</td>
</tr>


<tr>
<td class="org-left">mw-enumerate-visual-lines-relative-to-point</td>
<td class="org-left">command</td>
</tr>


<tr>
<td class="org-left">mw-enumerate-visual-lines-dist-to-border</td>
<td class="org-left">command</td>
</tr>


<tr>
<td class="org-left">mw-count-window-lines</td>
<td class="org-left">defun</td>
</tr>


<tr>
<td class="org-left">mw-visual-line-number-with-point</td>
<td class="org-left">defun</td>
</tr>
</tbody>
</table>


<a id="org7203ca0"></a>

## Code

    (defun mw-visual-line-number-with-point ()
      (let ((pt (save-excursion (forward-line 0) (point)))
            (n 0))
        (save-excursion
          (while (< (progn
                       (move-to-window-line n)
                       (point))
                     pt)
            (setq n (1+ n))))
        n))

    (defun mw-ephermal-count-window-lines ()
      "Count the lines of the window."
      (let ((pt (save-excursion (move-to-window-line -1) (point)))
            (n 0))
        (save-excursion
          (while (< (progn
                       (move-to-window-line n)
                       (point))
                     pt)
            (setq n (1+ n))))
        n))


<a id="orge485148"></a>

## Links

linum-mode


<a id="orgb1132fd"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:31] </span></span> Using this occasionally.


<a id="orgbc31de1"></a>

# Split window below point


<a id="org04a6549"></a>

## Code

    (defun mw-visual-line-number ()
      "Visual line number of the line containing point."
      (save-excursion
        (let ((end-point (progn (beginning-of-visual-line) (point)))
              (count 1)
              (line-move-visual t))
          (move-to-window-line (1- count))
          (while (< (point) end-point)
            (incf count)
            (next-line))
          count)))

    (defun mw-split-window-vertically-at-point ()
      "Split window vertically at point."
      (interactive)
      (let ((split-point (point)))
        (save-excursion
          (save-restriction
            (split-window-below (1+ (mw-visual-line-number)))
            (goto-char split-point)
            (recenter -1)))))


<a id="orgac345df"></a>

# Horizontally Split window below point


<a id="orgaa553db"></a>

## Code

    (defun mw-visual-column-number ()
      "Visual column number of point."
      (save-excursion
        (let ((end-point (point)))
          (beginning-of-line)
          (1+ (- end-point (point))))))

    (defun mw-split-window-horizontally-at-point ()
      "Split window vertically at point."
      (interactive)
      (let ((split-point (point)))
        (save-excursion
          (save-restriction
            (split-window-right (mw-visual-column-number))))))


<a id="org8c6bd9f"></a>

# Kill outline-path


<a id="org8a51fd5"></a>

## Code

    (defun mw-org-kill-new-outline-path ()
      "Add the outline path to where point is to the ‘kill-ring’."
      (interactive)
      (let ((outline-path (org-display-outline-path t (not (org-before-first-heading-p)) nil t)))
        (kill-new outline-path)
        (message "'%s' added to kill-ring" outline-path)))


<a id="orgbf77ac5"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:32] </span></span> Using this sometimes before using org-refile
when I know to where the refile shall go.  At the refile I have just
to yank the outline-path.


<a id="org7362685"></a>

# Append to scratch


<a id="org018fb75"></a>

## Code

    (defun mw-append-to-scratch (beg end)
      "Append region to buffer scratch."
      (interactive "r")
      (append-to-buffer (get-buffer-create "*scratch*") beg end))


<a id="orgcc65dc1"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:33] </span></span> Using this occasionally.


<a id="orgc1942bd"></a>

# Buddy buffer


<a id="orge73bd13"></a>

## Code

    (defvar *mw-buddy-buffer* (current-buffer))

    (defun mw-exchange-to-buddy ()
      (interactive)
      (let ((other (current-buffer)))
        (and (buffer-live-p *mw-buddy-buffer*)
             (not (eq other *mw-buddy-buffer*))
             (switch-to-buffer *mw-buddy-buffer*))
        (if (eq other *mw-buddy-buffer*)
            (message "Already on buddy buffer.  '%s' remains buddy buffer."
                     (buffer-name *mw-buddy-buffer*))
          (setq *mw-buddy-buffer* other)
          (message "'%s' is buddy buffer now." (buffer-name *mw-buddy-buffer*)))))


<a id="org9e11cc2"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:33] </span></span> Using this occasionally.  Actually I thought
this would be a killer feature but it is not for me.


<a id="org9fa452a"></a>

# Umlautify


<a id="orgdda680b"></a>

## Code

    (defun mw-umlautify-before-point ()
      "Change character before point to its umlaut."
      (interactive)
      (let* ((conversion-table
             '((?a . "ä")
               (?o . "ö")
               (?u . "ü")
               (?s . "ß")
               (?A . "Ä")
               (?O . "Ö")
               (?U . "Ü")))
            (conversion (assoc (char-before) conversion-table)))
        (when conversion
              (delete-char -1)
              (insert (cdr conversion)))))


<a id="org6658666"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:34] </span></span> Good.  Using it often.


<a id="org69a9d56"></a>

# Kill Buffer Filename


<a id="org03f3ddc"></a>

## Code

    (defun mw-kill-buffer-filename ()
      "Append buffer-filename to ‘kill-ring’."
      (interactive)
      (when buffer-file-name
        (kill-new buffer-file-name)
        (message "%s entered kill-ring" buffer-file-name)))


<a id="org264ccc7"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:35] </span></span> Using this almost never.


<a id="org80a2329"></a>

# Display all appointments


<a id="orgb45bcd5"></a>

## About

Display all appointments of the Emacs calendar.  This function
completes the appt functions AFAICT.


<a id="org8b7ce59"></a>

## Code

    (defun mw-appt-display ()
    "List all appointments."
      (interactive)
      (let ((appt-list appt-time-msg-list))
        (with-current-buffer (get-buffer-create "*appts*")
          (setq buffer-read-only nil)
          (erase-buffer)
          (while appt-list
            (insert (prin1-to-string
                     (substring-no-properties (cadar appt-list) 0)) "\n")
            (setq appt-list (cdr appt-list)))))
      (switch-to-buffer "*appts*")
      (view-mode)
      (message "Get rid of this buffer pressing 'C'.  (Hint: view-mode)"))


<a id="org798d699"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:35] </span></span> Using this occasionally.


<a id="orgff996b0"></a>

# sha1 of region


<a id="org5e00c22"></a>

## Code

    (defun mw-sha1-of-region-to-kill-ring (start end)
      "See sha1 docu for the parameters."
      (interactive "r")
      (kill-new (sha1 (current-buffer) start end)))


<a id="org3f148ce"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:36] </span></span> Not using this often but I <span class="underline">think</span> this has
potential.


<a id="org305afcb"></a>

# Insert Random Name


<a id="org6c01760"></a>

## About

Save time to find a name if all you want is a name.


<a id="orgdeef79d"></a>

## Code

    (defun mw-random-name ()
      "Insert a random name at point."
      (interactive)
      (insert "name_" (format "%019d" (abs (random)))))

    (defun name-0040310557124765049-test ()
      (let ((a 23))
        (while (< 0 a)
          (mw-random-name)
          (insert "\n")
          (setq a (- a 1)))))


<a id="orgbeee554"></a>

## Random

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:47] </span></span> I see potential.


<a id="orgcc04b04"></a>

# Extra Point


<a id="orge28f91c"></a>

## About

The feature Extra Point is just one extra location you can set or go
to.

A typical scenario for Extra Point is given when you are at a location
and you feel you want to go somewhere else but you are quite sure you
want to come back to the current location.


<a id="org29877ce"></a>

## Interface

Set the Extra Point with

    M-x mw-set-extra-point

Go to the Extra Point with

    M-x mw-goto-extra-point


<a id="orgb30247e"></a>

## Code

    (defvar *mw-extra-point* nil)

    (defun mw-set-extra-point ()
      "Save current position as Extra Position."
      (interactive)
      (setf *mw-ariadne-point* (cons (current-buffer) (point)))
      (message "Extra point has been set."))

    (defun mw-goto-extra-point ()
      "Set current cursor to the Extra Point if possible."
      (interactive)
      (when *mw-ariadne-point*
        (if (buffer-live-p (car *mw-ariadne-point*))
            (progn
              (switch-to-buffer (car *mw-ariadne-point*))
              (goto-char (cdr *mw-ariadne-point*))
              (message "At current Extra Point."))
          (message "Warning: Extra Point not found."))))


<a id="orgcad2c08"></a>

## Use case

One user set these commands to key-chords and uses them occasionally.


<a id="orgc30e792"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:47] </span></span> Using occasionally.




<a id="orga9309bf"></a>

# Switch external minibuffer     :lab:


<a id="org5c52d27"></a>

## About

This is for setting the minibuffer into an extra frame.

Get back a minibuffer in the frame using `M-x
mw-set-minibuffer-in-default-frame` `C-x 5 2` `C-x 5 1`.


<a id="orgf8ea6b5"></a>

## Code

    (defun mw-unset-minibuffer-in-default-frame ()
      (interactive)
      (setf (cdr (assoc 'minibuffer default-frame-alist)) nil))

    (defun mw-set-minibuffer-in-default-frame ()
      (interactive)
      (if (assoc 'minibuffer default-frame-alist)
          (setf (cdr (assoc 'minibuffer default-frame-alist)) t)
        (setf default-frame-alist
              (append (list (cons 'minibuffer t )) default-frame-alist))))


<a id="org51afb84"></a>

## Rating

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:36] </span></span> Not using this often but I think this has
potential.  Possibly this is also too buggy still.

<span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:43] </span></span> This looks broken.

<span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:27] </span></span> This looks good.  I like this extra minibuffer
frame setting somehow.


<a id="org9f3b403"></a>

# Hours in the current day


<a id="orgbc837aa"></a>

## About

Time of day as float.


<a id="orgc43857d"></a>

## Code

    (defun mw-current-time-of-day-decimal ()
      "Return the decimal hours of the time of day.
    E.g. 11.9 is almost noon, 12.0 is noon.
    "
      (let ((time (current-time)))
        (+ (string-to-number (format-time-string "%H" time))
           (/ (string-to-number (format-time-string "%M" time)) 60.0))))


<a id="orga65e04c"></a>

## Rating

-   <span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:35] </span></span> Forgotten about this function.
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:30] </span></span> Again ;)


<a id="org3546eed"></a>

# Exterminate top of kill ring


<a id="org6410235"></a>

## Code

    (defun mw-kill-ring-exterminate-top ()
      "Exterminate the current element from the `kill-ring'."
      (interactive)
      (setq kill-ring (cdr kill-ring))
      (setq kill-ring-yank-pointer kill-ring))


<a id="org3de9cb0"></a>

## Rating

-   <span class="timestamp-wrapper"><span class="timestamp">[2016-04-28 Thu 22:34] </span></span> Not using this.
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-10-11 Tue 11:15] </span></span> Just used this function.
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:32] </span></span> So at least one use!


<a id="org23c0c7f"></a>

# Freeze Emacs Time     :lab:


<a id="org625e068"></a>

## About


<a id="orgea39822"></a>

### Analogy

Like in those SF films when the time stands still and the hero can
manipulate everything.


<a id="orgcf2bd00"></a>

### Notes

;;
Note that this freeze here pertains not every time-source of Emacs.
AFAIHS `(format-time-string "%X")` gets its data somewhere else.


<a id="org3374441"></a>

## Code

    (let (freeze-time-day)
      (defun freeze-time-adviser (x)
        (append (date-to-time (concat freeze-time-day " 11:55")) (list 0 0)))

      (defun freeze-time-unfreeze ()
        (interactive)
        (if (advice-member-p #'freeze-time-adviser #'current-time)
            (advice-remove #'current-time #'freeze-time-adviser)))

      (defun freeze-time-to (yyyy-mm-dd-date-string)
        "Advice `current-time' to return time YYYY-MM-DD-DATE-STRING at 11:55am."
        (interactive (list (org-read-date)))
        (freeze-time-unfreeze)
        (setf freeze-time-day yyyy-mm-dd-date-string)
        (advice-add #'current-time :filter-return #'freeze-time-adviser)))


<a id="orgdc51e06"></a>

## Rating

-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:32] </span></span> Using this occasionally to change history.


<a id="org62498ff"></a>

# Refile to known location     :lab:


<a id="orgba1f125"></a>

## About

Use bookmark \`org-refile-direct-target' as target for the refile.

This functionality is for saving the time to wait for the
completion of the possible candidates for the case when you already
know where the refile should go.

Of course you must have set the target first.  This means setting
bookmark \`org-refile-direct-target'.  This can be done with
function \`mw-org-refile-set-direct-target-bm' or by foot via
setting the apropriate bookmark.  E.g. at the target point set the
mark with C-x rm \`org-refile-direct-target'.

``M-x `mw-org-refile-refile-to-direct-target'`` refiles the current
outline to the \`org-refile-direct-target' bookmark.

Reminder: Get the list of bookmarks with C-x rl.
\`counsel-bookmark' is an alternative.


<a id="org872a3b3"></a>

## Code

    (defun mw-org-refile-set-direct-target-bm ()
      "Set to bookmark `org-refile-direct-target' to current point."
      (interactive)
      (bookmark-set "org-refile-direct-target")
      (message "bookmark org-refile-direct-target set"))

    (defun mw-org-refile-refile-to-direct-target ()
      "Refile to bookmark `org-refile-direct-target'."
      (interactive)
      (let ((file (bookmark-get-filename "org-refile-direct-target"))
            (pos (bookmark-get-position "org-refile-direct-target")))
        (if (not file)
            (message "Doing nothing.  Bookmark `org-refile-direct-target' not set")
          (org-refile nil (current-buffer) `(nil ,file nil ,pos)))))


<a id="org271e57c"></a>

## Rating

-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:35] </span></span> Used this occasionally.


<a id="org1863da8"></a>

# Ack for projectile


<a id="org4667aef"></a>

## About

Ack for projectile.


<a id="orgb00bda1"></a>

## Code

    (defun projectile-ack (search-term)
      "Run an ack search with SEARCH-TERM in the project."
      (interactive
       (list (read-from-minibuffer
              (projectile-prepend-project-name (format "Ack search for: "))
              (projectile-symbol-or-selection-at-point))))
      (if (require 'ack nil 'noerror)
          (ack (concat "ack " search-term) (projectile-project-root))
        (error "Package 'ack' is not available")))


<a id="org584146c"></a>

## Rating

-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:36] </span></span> This is not necessary since there are very
    similar tools around.  E.g. 'ag'.


<a id="org2f0a761"></a>

# Wget


<a id="orgcaf680f"></a>

## About

Get a file from the net.

-   Scenaria:
    -   You have an address of a file in the web.
    -   You want a copy of that file in the current directory.


<a id="orgc39d44f"></a>

## Code


<a id="orgcbf5adb"></a>

### wget with external wget

    (defun wget (url)
      "Download URL."
      (interactive (list (read-string "URL: ")))
      (call-process "wget" nil nil nil url))


<a id="org616b54c"></a>

### wget made in Emacs

Wget realized with Emacs' intrinsic powers.  This might be a weak
realization of wget.

    (defun raw-browse (url)
      "Fetch URL to a buffer."
      (interactive
       (list (read-string "URL: " nil nil nil)))
      (url-retrieve
       url
       (lambda (status url &optional point buffer encode)
         (message "url: %s, status: %s" url status)
         (let ((buffer-name (concat  "*raw " url "*")))
           (append-to-buffer
            buffer-name
            (point-min) (point-max))
           (switch-to-buffer buffer-name)))
       (list url nil (current-buffer))))


<a id="org96a3744"></a>

## Rating


<a id="org25ed1e4"></a>

# Repeat last command


<a id="orgf2c7bf5"></a>

## About

Repeat the last command of the extended command history.

The functionality is (of course) already long available in Emacs.  Do
e.g. M-x M-p RET and there you are.

So this section is about to shorten this already short dance.

This is very experimental.  E.g. breaks when choosing a command that
needs a parameter.


<a id="org6d2eeef"></a>

## Code

    (defun mw-repeat-last-command ()
      "Execute first command in `extended-command-history'."
      (interactive)
      (funcall-interactively
       (read
        (car
         (remove-if
          (lambda (x) (string= "mw-repeat-last-command" x))
          extended-command-history)))))

    (defun mw-message-last-command ()
      "Display the command which would be activated by `mw-repeat-last-command'."
      (interactive)
      (message
       (car (remove-if
             (lambda (x) (string= "mw-repeat-last-command" x))
             extended-command-history))))


<a id="orgd5bcde5"></a>

### fix code to only do the necessary


<a id="orgd49b753"></a>

## Rating

-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:44] </span></span> Using this often.  Bound to C-6.
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-05 Sat 15:45] </span></span> The command is fragile.  E.g. fails for
    commands which expect parameters.
